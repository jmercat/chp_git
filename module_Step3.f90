module Step3
use Shared

contains


!**************************
!STEP 3: VISCO-ELASTIC PART
!**************************
subroutine Step3_Strain_Update(mesh)
	implicit none
	type(cell),dimension(0:Nx+1,0:Ny+1),intent(inout)::mesh
	integer::i,j

	do j=1,Ny
		do i=1,Nx
			mesh(i,j)%V_Pa_strain=(1./(1+(dt/p_s_Tau))) * ( mesh(i,j)%V_Pa_strain +(2.*p_pas_ElasticViscosity*dt/p_s_Tau)*D(mesh,i,j) )
		end do
	end do
	
	!Update of the boundaries conditions
	call Boundaries_conditions(mesh)

end subroutine


function D(mesh,i,j) result(dv)
	implicit none
	type(cell),dimension(0:Nx+1,0:Ny+1),intent(in)::mesh
	real*8,dimension(3)::dv 
	integer,intent(in)::i,j

dv(1)=(mesh(i,j)%v_mps_Speed(1)-mesh(i-1,j)%v_mps_Speed(1) )/dx  

dv(2)=(mesh(i,j)%v_mps_Speed(2)-mesh(i,j-1)%v_mps_Speed(2) )/dy

!dv(3)=(1./2) * ( (mesh(i,j+1)%v_mps_Speed(1)-mesh(i,j)%v_mps_Speed(1) )/dy 
!& + (mesh(i+1,j)%v_mps_Speed(2)-mesh(i,j)%v_mps_Speed(2) )/dx )

dv(3)=(1./2) * ((mesh(i,j)%v_mps_Speed(1)+mesh(i-1,j)%v_mps_Speed(1)+mesh(i,j+1)%v_mps_Speed(1)+mesh(i-1,j+1)%v_mps_Speed(1))/4.&
& - (mesh(i,j)%v_mps_Speed(1)+mesh(i-1,j)%v_mps_Speed(1)+mesh(i,j-1)%v_mps_Speed(1)+mesh(i-1,j-1)%v_mps_Speed(1))/4.)/dy&
& + ((mesh(i,j)%v_mps_Speed(2)+mesh(i,j-1)%v_mps_Speed(2)+mesh(i-1,j)%v_mps_Speed(2)+mesh(i-1,j-1)%v_mps_Speed(2))/4.&
& - (mesh(i,j)%v_mps_Speed(2) + mesh(i+1,j)%v_mps_Speed(2) + mesh(i,j-1)%v_mps_Speed(2) + mesh(i+1,j-1)%v_mps_Speed(2) )/4. )/dx

end function

end module
