module solver


contains

subroutine bicgstab(k, b, x0,  x1, iter1)
implicit none
real*8, dimension(7), intent(in) :: k
real*8, dimension(:), intent(in) :: b, x0
real*8, dimension(:), pointer, intent(out) :: x1
integer, intent(out):: iter1
real*8, dimension(:), pointer :: Ax0, v, p, r, r0, s, t
real*8 :: rho0, tol, alpha, w0, beta, rho, w, nr
integer :: nt,n, j
nt=size(b)
n=floor(sqrt(real(nt-2)))

allocate(Ax0(nt), v(nt), p(nt), r(nt), r0(nt), s(nt), t(nt), x1(nt))



x1=x0
v=0
p=0


Ax0=Aproduct2(k, x0)


r0=b-Ax0
r=r0


rho0=1
alpha=1
w=1
tol=1.e-16
nr=norme(r0)
iter1=0
do while (nr>tol)
	iter1=iter1+1
	
	rho=dot_product(r0, r)
	
	beta=(rho/rho0)*(alpha/w) !!!alpha n? (n-1)

	do j=1, nt
		p(j)=r(j)+beta*(p(j)-w*v(j))
	enddo
	v=Aproduct2(k, p)
	
	alpha=rho/dot_product(r0, v)

	do j=1, nt
		s(j)=r(j)-alpha*v(j)
	enddo
	t=Aproduct2(k, s)! nul
	
	w=(dot_product(t, s)/dot_product(t, t))

	do j=1, nt
		x1(j)=x1(j)+alpha*p(j)+w*s(j)
		
		r(j)=s(j)-w*t(j)
	enddo
	nr=norme(r)
	
	

	rho0=rho

enddo
!-----------------------------------------------------------------------------------------------------------------
!x2=x0
!v=0
!p=0


!Ax0=Aproduct3(k, x0)


!r0=b-Ax0
!r=r0


!rho0=1
!alpha=1
!w=1
!tol=1.e-16
!nr=norme(r0)
!iter2=0
!do while (nr>tol)
!	iter2=iter2+1
!	
!	rho=dot_product(r0, r)
!	
!	beta=(rho/rho0)*(alpha/w) !!!alpha n? (n-1)
!
!	do j=1, nt
!		p(j)=r(j)+beta*(p(j)-w*v(j))
!	enddo
!	v=Aproduct2(k, p)
!	
!	alpha=rho/dot_product(r0, v)
!
!	do j=1, nt
!		s(j)=r(j)-alpha*v(j)
!	enddo
!	t=Aproduct2(k, s)! nul
!	
!	w=(dot_product(t, s)/dot_product(t, t))
!
!	do j=1, nt
!		x2(j)=x2(j)+alpha*p(j)+w*s(j)
!		r(j)=s(j)-w*t(j)
!	enddo
!	nr=norme(r)
!	
!	
!
!	rho0=rho
!
!enddo
end subroutine



!function Aproduct2(k,  x1) result(prod)
!implicit none
!real*8, dimension(7) :: k
!real*8, dimension(:), pointer ::prod
!real*8, dimension(:) :: x1
!integer :: i, j, n, nt

!nt=size(x1)
!n=floor(sqrt(real(nt/2)))

!allocate(prod(nt))


!pour intégrer indicatrice, k e [1, n²], (i, j) e [1, n]², i=ceiling(k/n), j=k-(i-1)*n

!prod(1)=k(1)*x1(1)+k(2)*x1(2)+k(3)*x1(n+1)+k(4)*(x1(n**2+1)-x1(n**2+2))
!do i=2, n-1
!	prod(i)=k(1)*x1(i)+k(2)*(x1(i-1)+x1(i+1))+k(3)*x1(n+i)+k(4)*(x1(n**2+i)-x1(n**2+1+i))
!enddo
!prod(n)=k(1)*x1(n)+k(2)*x1(n-1)+k(3)*x1(2*n)+k(4)*x1(n**2+n)
!do i=n+1, n**2-n
!	if (modulo(i, n)==0) then
!		prod(i)=k(1)*x1(i)+k(2)*x1(i-1)+k(3)*(x1(i-n)+x1(n+i))+k(4)*(-x1(n**2-n+i)+x1(n**2+i))
!	elseif (modulo(i, n)==1) then
!		prod(i)=k(1)*x1(i)+k(2)*x1(i+1)+k(3)*(x1(i-n)+x1(n+i))+k(4)*(-x1(n**2-n+i)+x1(n**2-n+1+i)+x1(n**2+i)-x1(n**2+1+i))
!	else
!		prod(i)=k(1)*x1(i)+k(2)*(x1(i-1)+x1(i+1))+k(3)*(x1(i-n)+x1(n+i))+k(4)*(-x1(n**2-n+i)+x1(n**2-n+1+i)+x1(n**2+i)-x1(n**2+1+i))
!	endif
!enddo
!prod(n**2-n+1)=k(1)*x1(n**2-n+1)+k(2)*x1(n**2-n+2)+k(3)*x1(n**2-2*n+1)+k(4)*(-x1(nt-2*n+1)+x1(nt-2*n+2))
!do i=n**2-n+2, n**2-1!vitesse bord sup nulle
!	prod(i)=k(1)*x1(i)+k(2)*(x1(i-1)+x1(i+1))+k(3)*x1(i-n)+k(4)*(-x1(n**2-n+i)+x1(n**2-n+1+i))
!enddo
!prod(n**2)=k(1)*x1(n**2)+k(2)*x1(n**2-1)+k(3)*x1(n**2-n)+k(4)*(-x1(nt-n))
!prod(n**2+1)=k(5)*x1(n**2+1)+k(6)*x1(n**2+2)+k(7)*x1(n**2+1+n)+k(4)*(x1(1)-x1(n+1))
!do i=n**2+2, n**2+n-1
!	prod(i)=k(5)*x1(i)+k(6)*(x1(i-1)+x1(i+1))+k(7)*x1(i+n)+k(4)*(-x1(i-n**2-1)+x1(i-n**2)+x1(i-n**2-1+n)-x1(i-n**2+n))
!enddo
!prod(n**2+n)=k(5)*x1(n**2+n)+k(6)*x1(n**2+n-1)+k(7)*x1(n**2+2*n)+k(4)*(-x1(n-1)+x1(n)+x1(2*n-1)-x1(2*n))
!do i=n**2+n+1, nt-n
!	if (modulo(i, n)==0) then
!		prod(i)=k(5)*x1(i)+k(6)*x1(i-1)+k(7)*(x1(i-n)+x1(i+n))+k(4)*(-x1(i-n**2-1)+x1(i-n**2)+x1(i-n**2-1+n)-x1(i-n**2+n))
!	elseif (modulo(i, n)==1) then
!		prod(i)=k(5)*x1(i)+k(6)*x1(i+1)+k(7)*(x1(i-n)+x1(i+n))+k(4)*(x1(i-n**2)-x1(i-n**2+n))
!	else
!		prod(i)=k(5)*x1(i)+k(6)*(x1(i-1)+x1(i+1))+k(7)*(x1(i-n)+x1(i+n))+k(4)*(-x1(i-n**2-1)+x1(i-n**2)+x1(i-n**2-1+n)-x1(i-n**2+n))
!	endif
!enddo
!prod(nt-n+1)=k(7)*x1(nt-2*n+1)+k(4)*x1(n**2-n+1)
!do i=nt-n+2, nt-1
!	prod(i)=k(7)*x1(i-n)+k(4)*(-x1(i-n**2-1)+x1(i-n**2))
!enddo
!prod(nt)=k(7)*x1(nt-n)+k(4)*(-x1(n**2-1)+x1(n**2))


!end function



!-----------------------------------------------------------------------------------------------------------------------------------



!sans vitesse nulle bord sup
function Aproduct2(k,  x1) result(prod)
implicit none

real*8, dimension(7) :: k
real*8, dimension(:), pointer ::prod
real*8, dimension(:) :: x1
integer :: i, j, n, nt, col, row
real*8:: dx, dy, k1bis, k5bis

nt=size(x1)
n=floor(sqrt(real(nt/2)))
dx=0.1
dy=0.1
allocate(prod(nt))

!i=ceil(k/n)
!j=k-(i*1)n


!if (mask(1, 1)==1) then
	k1bis=k(1)
!else
!	k1bis=k(8)
!endif

prod(1)=k1bis*x1(1)+k(2)*x1(2)+k(3)*x1(n+1)+k(4)*(x1(n**2+1)-x1(n**2+2))



do i=2, n-1
!	row=ceiling(real(i/n))
!	col=i-(row-1)*n
!	print*, real(i/n)
!	if (mask(row, col)==1) then
		k1bis=k(1)
!	else
!		k1bis=k(8)
!	endif
	prod(i)=k1bis*x1(i)+k(2)*(x1(i-1)+x1(i+1))+k(3)*x1(n+i)+k(4)*(x1(n**2+i)-x1(n**2+1+i))
enddo


!if (mask(1, n)==1)then
	k1bis=k(1)
!else
!	k1bis=k(8)
!endif
prod(n)=k1bis*x1(n)+k(2)*x1(n-1)+k(3)*x1(2*n)+k(4)*x1(n**2+n)


do i=n+1, n**2-n
!	row=ceiling(real(i/n))
!	col=i-(row-1)*n
!	if (mask(row, col)==1) then
		k1bis=k(1)
!	else
!		k1bis=k(8)
!	endif
	
	if (modulo(i, n)==0) then
		prod(i)=k1bis*x1(i)+k(2)*x1(i-1)+k(3)*(x1(i-n)+x1(n+i))+k(4)*(-x1(n**2-n+i)+x1(n**2+i))
	elseif (modulo(i, n)==1) then
		prod(i)=k1bis*x1(i)+k(2)*x1(i+1)+k(3)*(x1(i-n)+x1(n+i))+k(4)*(-x1(n**2-n+i)+x1(n**2-n+1+i)+x1(n**2+i)-x1(n**2+1+i))
	else
		prod(i)=k1bis*x1(i)+k(2)*(x1(i-1)+x1(i+1))+k(3)*(x1(i-n)+x1(n+i))+k(4)*(-x1(n**2-n+i)+x1(n**2-n+1+i)+x1(n**2+i)-x1(n**2+1+i))
	endif
enddo

!row=ceiling(real((n**2-n+1)/n))
!col=n**2-n+1-(row-1)*n
!if (mask(row, col)==1) then
	k1bis=k(1)
!else
!	k1bis=k(8)
!endif
prod(n**2-n+1)=k1bis*x1(n**2-n+1)+k(2)*x1(n**2-n+2)+k(3)*x1(n**2-2*n+1)+k(4)*(-x1(nt-2*n+1)+x1(nt-2*n+2)+x1(nt-n+1)-x1(nt-n+2))


do i=n**2-n+2, n**2-1
!	row=ceiling(real(i/n))
!	col=i-(row-1)*n
!	if (mask(row, col)==1) then
		k1bis=k(1)
!	else
!		k1bis=k(8)
!	endif

	prod(i)=k1bis*x1(i)+k(2)*(x1(i-1)+x1(i+1))+k(3)*x1(i-n)+k(4)*(-x1(n**2-n+i)+x1(n**2-n+1+i)+x1(n**2+i)-x1(n**2+1+i))
enddo

!if (mask(n, n)==1) then
	k1bis=k(1)
!else
!	k1bis=k(8)
!endif
prod(n**2)=k1bis*x1(n**2)+k(2)*x1(n**2-1)+k(3)*x1(n**2-n)+k(4)*(-x1(nt-n)+x1(nt))




!if (mask(1, 1)==1) then
	k1bis=k(5)
!else
!	k1bis=k(9)
!endif
prod(n**2+1)=k5bis*x1(n**2+1)+k(6)*x1(n**2+2)+k(7)*x1(n**2+1+n)+k(4)*(x1(1)-x1(n+1))


do i=n**2+2, n**2+n-1
!	row=ceiling(real((i-n**2)/n))
!	col=(i-n**2)-(row-1)*n
!	if (mask(row, col)==1) then
		k5bis=k(5)
!	else
!		k5bis=k(9)
!	endif
	prod(i)=k5bis*x1(i)+k(6)*(x1(i-1)+x1(i+1))+k(7)*x1(i+n)+k(4)*(-x1(i-n**2-1)+x1(i-n**2)+x1(i-n**2-1+n)-x1(i-n**2+n))
enddo


!if (mask(1, n)==1) then
	k5bis=k(5)
!else
!	k5bis=k(9)
!endif
prod(n**2+n)=k5bis*x1(n**2+n)+k(6)*x1(n**2+n-1)+k(7)*x1(n**2+2*n)+k(4)*(-x1(n-1)+x1(n)+x1(2*n-1)-x1(2*n))


do i=n**2+n+1, nt-n
!	row=ceiling(real((i-n**2)/n))
!	col=i-n**2-(row-1)*n
!	if (mask(row, col)==1) then
		k5bis=k(5)
!	else
!		k5bis=k(9)
!	endif
	if (modulo(i, n)==0) then
		prod(i)=k5bis*x1(i)+k(6)*x1(i-1)+k(7)*(x1(i-n)+x1(i+n))+k(4)*(-x1(i-n**2-1)+x1(i-n**2)+x1(i-n**2-1+n)-x1(i-n**2+n))
	elseif (modulo(i, n)==1) then
		prod(i)=k5bis*x1(i)+k(6)*x1(i+1)+k(7)*(x1(i-n)+x1(i+n))+k(4)*(x1(i-n**2)-x1(i-n**2+n))
	else
		prod(i)=k5bis*x1(i)+k(6)*(x1(i-1)+x1(i+1))+k(7)*(x1(i-n)+x1(i+n))+k(4)*(-x1(i-n**2-1)+x1(i-n**2)+x1(i-n**2-1+n)-x1(i-n**2+n))
	endif
enddo

!row=ceiling(real((n**2-n+1)/n))
!col=n**2-n+1-(row-1)*n
!if (mask(row, col)==1) then
	k5bis=k(5)
!else
!	k5bis=k(9)
!endif
prod(nt-n+1)=k5bis*x1(nt-n+1)+k(6)*x1(nt-n+2)+k(7)*x1(nt-2*n+1)+k(4)*x1(n**2-n+1)



do i=nt-n+2, nt-1
!	row=ceiling(real((i-n**2)/n))
!	col=i-n**2-(row-1)*n
!	if (mask(row, col)==1) then
		k5bis=k(5)
!	else
!		k5bis=k(9)
!	endif
	prod(i)=k5bis*x1(i)+k(6)*(x1(i-1)+x1(i+1))+k(7)*x1(i-n)+k(4)*(-x1(i-n**2-1)+x1(i-n**2))
enddo


!if (mask(n, n)==1)then
	k5bis=k(5)
!else
!	k5bis=k(9)
!endif
prod(nt)=k1bis*x1(nt)+k(6)*x1(nt-1)+k(7)*x1(nt-n)+k(4)*(-x1(n**2-1)+x1(n**2))


end function

function norme(X) result(norm)
implicit none
real*8,dimension(:)::X
real*8::norm
integer::i
norm= 0
do i=1,size(X)
	norm=norm+X(i)**2
	norm=sqrt(norm)
end do

end function


end module
