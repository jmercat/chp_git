!******************************
!Méthode du Lagrangien Augmenté
!******************************
module Lagrangien
	use Shared
	use Step2
	contains
	!**************************************
	!Updates the mesh pressure and velocity 
	!**************************************
	subroutine Lagrangien_Augmente(mesh,ToL)
		implicit none 
		type(cell),dimension(0:Nx+1,0:Ny+1),intent(inout)::mesh
		real*8,dimension(:,:),allocatable::Pressure_old
		real*8::norme_Delta_Q,norme_Q
		real*8,intent(in)::ToL
		integer::i,j
		logical::condition
		
		allocate(Pressure_old(Nx,Ny))
		
		!The condition is initialy set to .FALSE.
		!When the criterion is verified, the subroutine is stopped
		condition=.FALSE.
		
		do while (.NOT. condition)
			!Storage of the pressure in pressure_old to calculate the 
			!convergence condition
			do j=1,Ny
				do i=1,Nx
					Pressure_old(i,j)=mesh(i,j)%v_pa_pressure
				end do
			end do
		
			!-------------------------------
			!Resolution of the linear system
			!-------------------------------
			call linear_system_resolution(mesh)
		
			!--------------------------------------
			!Pressure Update using the new velocity
			!--------------------------------------
			call Lagrangien_Pressure_Update(mesh)
				
			!Norme calculation 
			norme_Delta_Q=0
			norme_Q=0
			!norme_Delta_Q
			!Rectangle
			do j=1,Ny
				do i=1,Nx
					norme_Delta_Q=norme_Delta_Q+abs(mesh(i,j)%v_pa_pressure-Pressure_old(i,j))**2*dx*dy
				end do
			end do
			norme_Delta_Q=sqrt(norme_Delta_Q)
			
			do j=1,Ny
				do i=1,Nx
					norme_Q=norme_Q+abs(Pressure_old(i,j))**2*dy*dx
				end do
			end do
			norme_Q=sqrt(norme_Q)
			!print*,norme_Q
			!print*,norme_Delta_Q/norme_Q
			
			!Condition
			if(norme_Delta_Q/norme_Q<=ToL) then
				!print*,'Convergeance'
				condition=.TRUE.
			end if

		end do
		
		!Update the boundaries conditions at every step
		call Boundaries_conditions(mesh)	
		
		deallocate(Pressure_old)
		
	end subroutine
	
	subroutine Lagrangien_Pressure_Update(mesh)
		implicit none
		type(cell),dimension(0:Nx+1,0:Ny+1),intent(inout)::mesh
		integer::i,j
		real*8::Div
		
		do j=1,Ny
			do i=1,Nx
			Div=(mesh(i,j)%v_mps_speed(1)-mesh(i-1,j)%v_mps_speed(1))/dx + (mesh(i,j)%v_mps_speed(2)-mesh(i,j-1)%v_mps_speed(2))/dy
			mesh(i,j)%v_pa_pressure=mesh(i,j)%v_pa_pressure - p_pas_r*Div
			end do
		end do
	
	end subroutine
		
	
subroutine buildA(mesh,i,j, eq1, eq2)
	implicit none
	type(cell),dimension(0:Nx+1,0:Ny+1),intent(in)::mesh
	integer,intent(in):: i,j
	! 2 premières dimensions : -1 pour i-1 0 pour i 1 pour i+1 pareil sur j
	! 3e dimension : 1 pour U, 2 pour V
	real*8,dimension(-1:1,-1:1,2),intent(out)::eq1,eq2 

	eq1=0.d0
	eq2=0.d0
	!on utilise les mailles fictives pour les conditions aux bords
	!if (i<Nx+1 .and. j<Ny+1 .and. i>0 .and. j>0) then
			
		!Linear System
		beta = p_pas_Viscosity+p_pas_ElasticViscosity*dt/p_s_Tau
		
		k1 = -2*beta*(dy/dx+dx/dy)-2*p_pas_r*dy/dx
		k2 = (beta+p_pas_r)*dy/dx
		k3 = beta*dx/dy
		k4 = -p_pas_r
			
		k5 = -2*beta*(dy/dx+dx/dy)-2*p_pas_r*dx/dy
		k6 = beta*dy/dx
		k7 = (beta+p_pas_r)*dx/dy
	
		eq1(0,0,1) = k1-&
		&1/p_ppaps_eps*dx*dy*(1-mesh(i,j)%m_mask)*(1-mesh(i-1,j)%m_mask)+&
		&k3*logical2real(j==1)+k3*logical2real(j==Ny)+&
		&k2*logical2real(i==Nx)
			
		eq1(1,0,1) = k2*logical2real(i<Nx)
		eq1(-1,0,1) = k2
		eq1(0,1,1) = k3*logical2real(j<Ny)
		eq1(0,-1,1) = k3*logical2real(j>1)
		
		eq1(0,0,2) = (k4+k4*logical2real(j==1))*logical2real(i<Nx)
		eq1(1,0,2) = (-k4-k4*logical2real(j==1))*logical2real(i<Nx)
		eq1(0,-1,2) =-k4*logical2real(j>1)*logical2real(i<Nx)
		eq1(1,-1,2) = k4*logical2real(j>1)*logical2real(i<Nx)
		
		if (j<Ny) then
			eq2(0,0,2)=k5-1/p_ppaps_eps*dx*dy*&
			&(1-mesh(i,j)%m_mask)*(1-mesh(i,j-1)%m_mask)+&
			&k6*logical2real(i==Nx)+&
			&k7*logical2real(j==1)-k7*logical2real(j==Ny)
		else
			eq2(0,0,2)=k5-1D12+&
			&k6*logical2real(i==Nx)+&
			&k7*logical2real(j==1)-k7*logical2real(j==Ny)
		end if
		
		eq2(1,0,2) = k6*logical2real(i<Nx)
		eq2(-1,0,2) = k6
		eq2(0,1,2) = k7*logical2real(j<Ny)
		eq2(0,-1,2) = k7*logical2real(j>1)
		
		eq2(0,0,1) = k4+k4*logical2real(j==Ny)
		eq2(-1,0,1) = -k4-k4*logical2real(j==Ny)
		eq2(0,1,1) = -k4*logical2real(j<Ny)
		eq2(-1,1,1) = k4*logical2real(j<Ny)






!-----------------------
		!eq1(0,0,1) = -2*beta*(dy/dx+dx/dy)-2*p_pas_r*dy/dx-1/p_ppaps_eps*dx*dy*(1-mesh(i,j)%m_mask)*(1-mesh(i-1,j)%m_mask)
			
		!eq1(1,0,1) = (beta+p_pas_r)*dy/dx
		!eq1(-1,0,1) = (beta+p_pas_r)*dy/dx
		!eq1(0,1,1) = beta*dx/dy
		!eq1(0,-1,1) = beta*dx/dy
		
		!eq1(0,0,2) = -p_pas_r
		!eq1(1,0,2) = p_pas_r
		!eq1(0,-1,2) = p_pas_r
		!eq1(1,-1,2) = -p_pas_r
		
		!eq2(0,0,2)=-2*beta*(dy/dx+dx/dy)-2*p_pas_r*dx/dy-1/p_ppaps_eps*dx*dy*(1-mesh(i,j)%m_mask)*(1-mesh(i,j-1)%m_mask)
		!eq2(1,0,2) = beta*dy/dx
		!eq2(-1,0,2) = beta*dy/dx
		!eq2(0,1,2) = (beta+p_pas_r)*dx/dy
		!eq2(0,-1,2) = (beta+p_pas_r)*dx/dy
		
		!eq2(0,0,1) = -p_pas_r
		!eq2(-1,0,1) = p_pas_r
		!eq2(0,1,1) = p_pas_r
		!eq2(-1,1,1) = -p_pas_r
!------------------------		
		
	!end if
end subroutine

!Methode de Jacobi avec l'ecriture non stockée de la matrice
!##############################Non testé##############################""
subroutine Jacobi(mesh,u,v,prec,b1,b2)
	implicit none
		type(cell),dimension(0:Nx+1,0:Ny+1),intent(inout)::mesh
		real*8,dimension(Nx,Ny),intent(out)::u,v
		real*8,dimension(Nx,Ny,2)::res
		real*8,dimension(Nx,Ny)::b1,b2
		real*8,dimension(0:Nx+1,0:Ny+1)::up,vp
		real*8,dimension(Nx,Ny,2)::Ax
		real*8,intent(in)::prec
		real*8::r
		real*8,dimension(-1:1,-1:1,2)::eq1,eq2 
		integer::i,j,k,l

		
		up=2!mesh(:,:)%v_mps_Speed(1)
		vp=2!mesh(:,:)%v_mps_Speed(2)
		Ax = matmulA(mesh,up,vp)
		r= normeij(Ax(:,:,1)-b1)/normeij(up)+normeij(Ax(:,:,2)-b2)/normeij(vp)
		
		print*,r
		do while (r>prec)
			do j=1,Ny
				do i=1,Nx
					!b1(i,j)=get_second_membre_X_maille(mesh,i,j)
					!b2(i,j)=get_second_membre_Y_maille(mesh,i,j)
					call buildA(mesh, i, j, eq1, eq2) 
					u(i,j) = 0
					v(i,j) = 0
					do k = -1,1
						do l =-1,1
							if (k==0 .and. l==0) then
								if (i+k>0 .and. i+k<Nx+1 .and. j+l>0 .and. j+l<Ny+1) then	
									u(i,j) = u(i,j)-up(i+k,j+l)*eq1(k,l,1)-vp(i+k,j+l)*eq1(k,l,2)
									v(i,j) = v(i,j)-up(i+k,j+l)*eq2(k,l,1)-vp(i+k,j+l)*eq2(k,l,2)
								endif
							else
								if (i+k>0 .and. i+k<Nx+1 .and. j+l>0 .and. j+l<Ny+1) then	
									u(i,j) = u(i,j)-vp(i+k,j+l)*eq1(k,l,2)
									v(i,j) = v(i,j)-vp(i+k,j+l)*eq2(k,l,2)
								endif
							end if
						end do
					end do
					u(i,j)=(b1(i,j)+u(i,j))/eq1(0,0,1)
					v(i,j)=(b2(i,j)+v(i,j))/eq2(0,0,1)
				end do
			end do
			print*, u(1,1)/up(1,1)
			up(1:Nx,1:Ny)=u
			vp(1:Nx,1:Ny)=v
			
			Ax = matmulA(mesh,u,v)
			
			r= normeij(Ax(:,:,1)-b1)/normeij(u)+normeij(Ax(:,:,2)-b2)/normeij(v)
		end do	
end subroutine

 subroutine bicgstabJ(mesh,u,v,prec,b1,b2)
 	implicit none
 	type(cell),dimension(0:Nx+1,0:Ny+1),intent(in)::mesh
 	real*8,dimension(Nx,Ny),intent(inout)::u,v
 	real*8,intent(in)::prec
 	real*8,dimension(Nx,Ny,2)::res,res0
 	real*8,dimension(Nx,Ny)::b1,b2
 	real*8,dimension(Nx,Ny,2)::Ax,Ap,p,s,As
 	real*8::rho,rho1,alpha,omega,beta
 	integer::compteur
 	
 	compteur = 0
 	
 	Ax = matmulA(mesh,u,v)
 	res0(:,:,1)=b1-Ax(:,:,1)
 	res0(:,:,2)=b2-Ax(:,:,2)
 	res = res0
 	rho = 1
 	rho1 = rho
 	alpha = 1
 	omega = 1
 	
 	Ap = 0
 	p = 0
 	
 	do while(normeij(res(:,:,1))+normeij(res(:,:,2))>prec .or. compteur > 10000)
 		compteur = compteur +1
 		if (compteur == 10000) then
 			print*, "Convergence non atteinte dans bicgstabJ"
 		end if
 		rho1 =  prodscaij2(res0,res)
 		!print*, "rho1", rho1
 		beta = rho1/rho*alpha/omega
 		!print*, "beta", beta
 		p=res+beta*(p-omega*Ap)
 		!print*, "P(1)", p(1,1,1)
 		Ap = matmulA(mesh,p(:,:,1),p(:,:,2))
 		!print*,"res0.Ap",prodscaij2(res0,Ap)
 		alpha = rho1/prodscaij2(res0,Ap)
 		!print*, "alpha", alpha
 		s = res-alpha*Ap
 		!print*, "s(1)",s(1,1,1)
 		As = matmulA(mesh,s(:,:,1),s(:,:,2))
 		!print*, "As.As", prodscaij2(As,As)
 		!print*, "As.s", prodscaij2(As,s)
 		omega=prodscaij2(As,s)/prodscaij2(As,As)
 		!print*,"Omega", omega
 		u = u + alpha*p(:,:,1) + omega*s(:,:,1)
 		v = v + alpha*p(:,:,2) + omega*s(:,:,2)
 		res = s - omega*As
 		!print*, "res(1)", res(1,1,1)
 		rho = rho1
 		if (isnan(u(1,1))) then
 			print*, "Premier NAN", compteur
 		endif
 		!res0 = res
 	end do
 	
 	!print*, "Nombre d'iterations : ", compteur
 	
 end subroutine


real*8 function prodscaij2(v1,v2)
	real*8,dimension(:,:,:),intent(in)::v1,v2
	integer::n,m,i,j
	
	n = size(v1,1)
	m = size(v1,2)
	prodscaij2=0
	
	do j=1,m
		do i=1,n
			prodscaij2 = prodscaij2+v1(i,j,1)*v2(i,j,1)+v1(i,j,2)*v2(i,j,2)
		end do
	end do
end function

! norme 2 de vecteur ecrit sous forme de matrice
real*8 function normeij(A)
	implicit none
	real*8,dimension(:,:),intent(in)::A
	integer::n1,n2,i,j
	
	n1 = size(A,1)
	n2 = size(A,2)
	normeij=0
	do j=1,n2
		do i=1,n1
			normeij=normeij+A(i,j)**2
		end do
	end do
	normeij=sqrt(normeij)
end function

!norme 2 classique
real*8 function normei(v)
	implicit none
	real*8,dimension(:),intent(in)::v
	integer::n1,i
	
	n1 = size(v)
	normei=0
	do i=1,n1
			normei=normei+v(i)**2
	end do
	normei=sqrt(normei)
end function

function matmulA(mesh,x1,x2)
	implicit none
		type(cell),dimension(0:Nx+1,0:Ny+1),intent(in)::mesh
		real*8,dimension(1:Nx,1:Ny),intent(in)::x1,x2
		real*8,dimension(Nx,Ny,2)::matmulA
		real*8,dimension(-1:1,-1:1,2)::eq1,eq2 
		integer::i,j,k,l
		matmulA=0.d0
		
		do i=1,Nx
			do j=1,Ny
				call buildA(mesh,i,j,eq1, eq2)
				
				do k = -1,1
					do l =-1,1
						if (i+k>0 .and. i+k<Nx+1 .and. j+l>0 .and. j+l<Ny+1) then	
								
								matmulA(i,j,1) = matmulA(i,j,1)+eq1(k,l,1)*x1(i+k,j+l)+&
											eq1(k,l,2)*x2(i+k,j+l)
								matmulA(i,j,2) = matmulA(i,j,2)+eq2(k,l,1)*x1(i+k,j+l)+&
											eq2(k,l,2)*x2(i+k,j+l)
						end if
					end do
				end do
			end do
		end do	
end function


!_____________________________________________________________________________________________________________

!~ !---> CALCUL DU SECOND MEMBRE /X SUR LA MAILLE (i,j), B1(i,j)<---!


!!Rappel :  le second membre est B, vecteur de taille 2*Nx*Ny. Les Nx*Ny premieres composantes forment le sous vecteur B1 et resultent d'une projection selon X du
!!second membre de l'equation vectorielle. Les Nx*Ny dernieres composantes forment le sous vecteur B2 et resultent d'une projection selon Y du
!!second membre de l'equation vectorielle. 



!~ !!ARGUMENTS D'ENTREE -> mesh ( vecteur de cells de taille (Nx+2) x (Ny+2) 
!~ !!SORTIE	                        -> B1(i,j) = un r�el = valeur du second membre selon X sur la maille (i,j)


!/!\ CETTE FONCTION FAIT APPEL A SIGMA^(n+2/3), IL FAUT DONT Y FAIRE APPEL UNE FOIS CETTE QUANTITE CALCULEE


function Get_Second_Membre_X_maille(mesh,i,j) result (B1)
implicit none 

  type(cell),dimension(0:Nx+1,0:Ny+1),intent(in)::mesh
  integer::i,j
  Real*8::B1,gamma,I1,I2,stilde1,stilde2,bb1,bb2 !bb1 et bb2 sont les valeurs rajoutées au second membre du fait des problèmes de bord
  
  stilde1=Get_Sigma_Tilde_12(mesh,i,j)
	
  stilde2=Get_Sigma_Tilde_12(mesh,i,j-1)
  
  gamma=1./(1.+dt/p_s_Tau)
  
  !AJOUT PERSONNEL
  !Pénalisation du second membre
	if (mesh(i,j)%m_mask==1) then
	  I1=-gamma*( dy*mesh(i+1,j)%V_Pa_Strain(1) - dy*mesh(i,j)%V_Pa_Strain(1) + dx*stilde1 - dx*stilde2)
	else
	  I1=-gamma*( dy*mesh(i+1,j)%V_Pa_Strain(1) - dy*mesh(i,j)%V_Pa_Strain(1) + dx*stilde1 - dx*stilde2)*p_ppaps_eps
	end if
  !/!\ le calcul de I2 fait intervenir la pression a l'iteration k ecrasee dans mesh
  
  I2=dy*(mesh(i+1,j)%v_pa_pressure - mesh(i,j)%v_pa_pressure)
  
  !call border_problems(mesh,i,j,bb1,bb2)
  B1=I1+I2!-bb1
end function


!_____________________________________________________________________________________________________________

!~ !---> CALCUL DU SECOND MEMBRE /Y SUR LA MAILLE (i,j), B2(i,j)<---!


!~ !!ARGUMENTS D'ENTREE -> mesh ( vecteur de cells de taille (Nx+2) x (Ny+2) 
!~ !!SORTIE	                        -> B2(i,j) = un reel = valeur du second membre selon Y sur la maille (i,j)


!/!\ CETTE FONCTION FAIT APPEL A SIGMA^(n+2/3), IL FAUT DONT Y FAIRE APPEL UNE FOIS CETTE QUANTITE CALCULEE


function Get_Second_Membre_Y_maille(mesh,i,j) result (B2)
implicit none 

  type(cell),dimension(0:Nx+1,0:Ny+1),intent(in)::mesh
  integer::i,j
  Real*8::B2,gamma,I1,I2,stilde1,stilde2,bb1,bb2
  
  stilde1=Get_Sigma_Tilde_12(mesh,i,j)
  stilde2=Get_Sigma_Tilde_12(mesh,i-1,j)
  
  gamma=1./(1.+dt/p_s_Tau)
  
  !AJOUT PERSONNEL
  !Pénalisation du second membre
	if (mesh(i,j)%m_mask==1) then
	  I1=-gamma*( dx*mesh(i,j+1)%V_Pa_Strain(2) - dx*mesh(i,j)%V_Pa_Strain(2) + dy*stilde1 - dy*stilde2)
	else
	  I1=-gamma*( dx*mesh(i,j+1)%V_Pa_Strain(2) - dx*mesh(i,j)%V_Pa_Strain(2) + dy*stilde1 - dy*stilde2)*p_ppaps_eps
	endif
	
  !/!\ le calcul de I2 fait intervenir la pression a l'iteration k ecrasee dans mesh
  
	I2=dx*(mesh(i,j+1)%v_pa_pressure - mesh(i,j)%v_pa_pressure)
	
  !call border_problems(mesh,i,j,bb1,bb2)
  B2=I1+I2!-bb2
end function

subroutine border_problems(mesh,i,j,v1,v2)
	implicit none
		type(cell),dimension(0:Nx+1,0:Ny+1),intent(in)::mesh
		real*8,dimension(-1:1,-1:1,2)::eq1,eq2
		real*8::v1,v2
		integer::i,j,k,l
		
		call buildA(mesh,i,j,eq1, eq2)
		v1 = 0
		v2 = 0
		do k=-1,1
			do l=-1,1
				v1 = v1 + mesh(i+k,j+l)%v_mps_Speed(1)*eq1(k,l,1)*&
				logical2real(i+k==Nx+1 .or. i+k == 0 .or. j+l==Ny+1 .or. j+l==0)&
				&+ mesh(i+k,j+l)%v_mps_Speed(2)*eq1(k,l,2)*&
				logical2real(i+k==Nx+1 .or. i+k == 0 .or. j+l==Ny+1 .or. j+l==0)
				
				v2 = v2 + mesh(i+k,j+l)%v_mps_Speed(2)*eq2(k,l,1)*&
				logical2real(i+k==Nx+1 .or. i+k == 0 .or. j+l==Ny+1 .or. j+l==0)&
				&+ mesh(i+k,j+l)%v_mps_Speed(2)*eq2(k,l,2)*&
				logical2real(i+k==Nx+1 .or. i+k == 0 .or. j+l==Ny+1 .or. j+l==0)
			end do
		end do
			
end subroutine

real*8 function logical2real(l)
	implicit none
	logical::l
	if (l) then
		logical2real = 1.d0
	else
		logical2real = 0.d0
	endif
end function

subroutine linear_system_resolution(mesh)
	implicit none
	type(cell),dimension(0:Nx+1,0:Ny+1),intent(inout)::mesh
	real*8,dimension(:,:,:),allocatable::secB
	real*8::prec
	integer::i,j
	allocate(secB(Nx,Ny,2))
	
	!second member b
	do j=1,Ny
		do i=1,Nx
			secB(i,j,1)=Get_Second_Membre_X_maille(mesh,i,j)
			secB(i,j,2)=Get_Second_Membre_Y_maille(mesh,i,j)
		end do
	end do
	
	!Solve Ax=b
	
	call bicgstabJ(mesh,mesh(1:Nx,1:Ny)%v_mps_speed(1),mesh(1:Nx,1:Ny)%v_mps_speed(2),Prec_solveur,secB(:,:,1),secB(:,:,2))
	
end subroutine
	
		
end module
