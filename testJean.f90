program NS_Code

	!The modul functions defines the main functions (Transport...)
	use Init
	use Step1
	use Step2
	use Lagrangien
	use Step3
	use Gif
	use Mask
	!use lagrangien
	implicit none 
	
	real*8,dimension(:,:),allocatable::x1,x2,u,v
	real*8,dimension(:,:,:),allocatable::secB
	real*8::prec
	real*8,dimension(-1:1,-1:1,2)::eq1,eq2 
	integer::i,j
	call Init_parameters()
	allocate(mesh(0:Nx+1,0:Ny+1),x1(Nx,Ny),u(Nx,Ny),v(Nx,Ny),x2(Nx,Ny),secB(Nx,Ny,2))
	call Initial_conditions(mesh)
	!x1=1
	!x2=1
	!secB = matmulA(mesh,x1,x2)
	do j=1,Ny
		do i=1,Nx
			secB(i,j,1)=Get_Second_Membre_X_maille(mesh,i,j)
			secB(i,j,2)=Get_Second_Membre_Y_maille(mesh,i,j)
		end do
	end do
!~ 	
	!print*, secB

 	!do i=1,Nx
 	!	do j=1,Ny
			!x1(i,j)=100*rand()
			!x2(i,j)=100*rand()
	!		call buildA(mesh,i,j, eq1, eq2)
			!print*, "est a diag strct dominante", abs(eq1(0,0,1))>sum(abs(eq1(:,:,1)))
			!write(*,'(18F3.0)') eq1
			!print*, sum(eq1)
			!print*, secB(i,j,1)
	!	end do
	!end do
	
	
	prec=1e-5
	call bicgstabJ(mesh,u,v,prec,secB(:,:,1),secB(:,:,2))
	print*, maxval(x1-u),maxval(x2-v)
end program
