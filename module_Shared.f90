module Shared
use variables

contains

!***********************************************************
!                 Function Boundaries_conditions
!***********************************************************
!Update the boundaries conditions of mesh
subroutine Boundaries_conditions(mesh)
implicit none
type(cell),dimension(0:Nx+1,0:Ny+1),intent(inout)::mesh
integer::i,j

	!**********
	!Top border
	!**********
	do i=1,Nx
		mesh(i,Ny+1)%v_mps_speed(1)=-mesh(i,Ny)%v_mps_speed(1)
		mesh(i,Ny+1)%v_mps_speed(2)=0
		mesh(i,Ny+1)%v_pa_pressure=mesh(i,Ny)%v_pa_pressure
		mesh(i,Ny+1)%v_pa_strain(:)=mesh(i,Ny)%v_pa_strain(:)
	end do

	!************
	!Right border
	!************
	do j=1,Ny
		mesh(Nx+1,j)%v_mps_speed(1)=mesh(Nx,j)%v_mps_speed(1)
		mesh(Nx+1,j)%v_mps_speed(2)=mesh(Nx,j)%v_mps_speed(2)
		mesh(Nx+1,j)%v_pa_pressure=mesh(Nx,j)%v_pa_pressure
		mesh(Nx+1,j)%v_pa_strain(:)=mesh(Nx,j)%v_pa_strain(:)
	end do
	
	!************
	!Left border
	!************
	do j=1,Ny
		mesh(0,j)%v_pa_pressure=mesh(1,j)%v_pa_pressure
		mesh(0,j)%v_pa_strain(:)=mesh(1,j)%v_pa_strain(:)
	end do
	
	!*************
	!Bottom border
	!*************
	do i=1,Nx
		mesh(i,0)%v_mps_speed(1)=-mesh(i,1)%v_mps_speed(1)
		mesh(i,0)%v_mps_speed(2)=0
		mesh(i,0)%v_pa_pressure=mesh(i,1)%v_pa_pressure
		mesh(i,0)%v_pa_strain(:)=mesh(i,1)%v_pa_strain(:)
	end do
	
	!******************
	!Bottom left corner
	!******************
	mesh(0,0)%v_mps_speed(1)=-mesh(0,1)%v_mps_speed(1)
	mesh(0,0)%v_mps_speed(2)=0
	mesh(0,0)%v_pa_pressure=0
	mesh(0,0)%v_pa_strain(:)=0
	
	!******************
	!Bottom right corner
	!******************
	mesh(Nx+1,0)%v_mps_speed(1)=-mesh(Nx+1,1)%v_mps_speed(1)
	mesh(Nx+1,0)%v_mps_speed(2)=0
	mesh(Nx+1,0)%v_pa_pressure=0
	mesh(Nx+1,0)%v_pa_strain(:)=0
	
	!***************
	!Top left border
	!***************
	mesh(0,Ny+1)%v_mps_speed(1)=-mesh(0,Ny)%v_mps_speed(1)
	mesh(0,Ny+1)%v_mps_speed(2)=0
	mesh(0,Ny+1)%v_pa_pressure=0
	mesh(0,Ny+1)%v_pa_strain(:)=0
	
	!***************
	!Top right border
	!***************
	mesh(Nx+1,Ny+1)%v_mps_speed(1)=-mesh(Nx,Ny)%v_mps_speed(1)
	mesh(Nx+1,Ny+1)%v_mps_speed(2)=0
	mesh(Nx+1,Ny+1)%v_pa_pressure=0
	mesh(Nx+1,Ny+1)%v_pa_strain(:)=0


end subroutine
!-----------------------------

!***********************************************************
!                 Function Find_TimeStep
!***********************************************************
!Calculate the time step dt with CFL condition
subroutine Find_TimeStep(mesh,dt)
implicit none
	!variable declaration 
	!in
	type(cell),dimension(:,:),allocatable,intent(in)::mesh
	!out
	real*8,intent(inout)::dt
if(abs(maxabsSpeed(mesh,1))>0 .OR. abs(maxabsSpeed(mesh,2))>0) then
	dt = 1./(maxabsSpeed(mesh,1)/dx+maxabsSpeed(mesh,2)/dy)
	!The time step is divided by 2 in order to have an homogeneous diffusion 
	!effect in all cases (so the CFL is allways underestimated).
	!dt=dt/2.
	
	!limitor
	!if(dt>dt_max) then 
	!	dt=dt_max
	!end if
else
	!limitor
	dt=dt_min
end if
	
end subroutine

!Calculates the inflow and outflow
subroutine flow(mesh)
implicit none
type(cell),dimension(0:Nx+1,0:Ny+1),intent(in)::mesh
real*8::Dv_in,Dv_out
integer::j

Dv_in=0
Dv_out=0

do j=1,Ny
	Dv_in=Dv_in+mesh(0,j)%v_mps_speed(1)
end do
Dv_in=Dv_in*dy

do j=1,Ny
	Dv_out=Dv_out+mesh(Nx,j)%v_mps_speed(1)
end do
Dv_out=Dv_out*dy

print*,'Dv_in=',Dv_in,'  ','DV_out=',Dv_out
end subroutine


!------------------------------------------------------------------------------------------
!Returns the absolute maximum speed in direction k (k=1 along x, k=2 y)
function maxabsSpeed(mesh,k)
implicit none
	real*8::maxabsSpeed
	type(cell),dimension(:,:),allocatable,intent(in)::mesh
	integer::i,j,k

	maxabsSpeed = abs(mesh(1,1)%V_mps_speed(k))
	do j=1,Ny
		do i=1,Nx
			if (maxabsSpeed<abs(mesh(i,j)%V_mps_speed(k))) then
				maxabsSpeed = abs(mesh(i,j)%V_mps_speed(k))
			end if
		end do
	end do
end function
!-----------------------------------------------------------------------------------
end module
