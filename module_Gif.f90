module gif
use variables
use Shared
contains

!***********************************************************
!               Function GIF_Frame
!***********************************************************
!Set the Gif parameters
subroutine GIF_Settings(GIF_parameter,Frame_Number,Frame_max,GIF_plot)
	implicit none
	integer,intent(out)::GIF_parameter,Frame_Number
	integer,intent(out),dimension(6)::GIF_plot
	integer,intent(out)::Frame_max
	!*********************************
	!GIF OPTIONS:
	!
	!-SELECT THE PLOT TYPE
	!plot_type=1 for a 3D plot
	!plot_typr=2 for a 2D plot with colour
	!plot_typr=3 for a 2D plot with vectors
	plot_type=3
	!
	!-SELECT THE NUMBER OF FRAMES
	Frame_Number=300
	!-SELECT THE PARAMETER TO ANIMATE
	!GIF_parameter=1 for the pressure animation
	!GIF_parameter=2 for the velocity over x animation
	!GIF_parameter=3 for the velocity over y animation
	!GIF_parameter=4 for sigma_11 animation
	!GIF_parameter=5 for sigma_22 animation
	!GIF_parameter=6 for sigma_12 animation
	GIF_parameter=2
	
		!-SELECT THE PARAMETERS TO ANIMATE (Select 1 for a GIF)
	!GIF_parameter1 for the pressure animation
	!GIF_parameter2 for the velocity over x animation
	!GIF_parameter3 for the velocity over y animation
	!GIF_parameter4 for sigma_11 animation
	!GIF_parameter5 for sigma_22 animation
	!GIF_parameter6 for sigma_12 animation
	GIF_plot(1)=1
	GIF_plot(2)=1
	GIF_plot(3)=1
	GIF_plot(4)=1
	GIF_plot(5)=1
	GIF_plot(6)=1
	
	!-Select the FPS option (1<=99)
	Delay=5
	!*********************************
	!Creates the GIF_temporary Directory
	!CALL System('mkdir GIF_temporary')
	
	if(GIF_plot(1)==1) then
		CALL System('mkdir GIF_temporary1')
	end if
	if(GIF_plot(2)==1) then
		CALL System('mkdir GIF_temporary2')
	end if
	if(GIF_plot(3)==1) then
		CALL System('mkdir GIF_temporary3')
	end if
	if(GIF_plot(4)==1) then
		CALL System('mkdir GIF_temporary4')
	end if
	if(GIF_plot(5)==1) then
		CALL System('mkdir GIF_temporary5')
	end if
	if(GIF_plot(6)==1) then
		CALL System('mkdir GIF_temporary6')
	end if

	
end subroutine
!Creates GIF data
subroutine GIF_Frame(GIF_parameter,Frame_Number,Frame_max)
	implicit none
	integer::i,j
	integer,intent(in)::GIF_parameter,Frame_Number
	integer,intent(inout)::Frame_max
	!Contains the values of the mesh points
	real*8,dimension(0:Nx+1)::X
	real*8,dimension(0:Ny+1)::Y
	
	!Creation of a character using on integer:
	!len=1 for the numbers [0:9]
	character(len=1)::number1
	!len=2 for the numbers [10:99]
	character(len=2)::number2
	!len=3 for the numbers [100:999]
	character(len=3)::number3
	!len=4 for the numbers [1000:9999]
	character(len=4)::number4
	
	do i=0,Nx+1
		X(i)=i*dx
	end do
	
	do i=0,Ny+1
		Y(i)=i*dy
	end do
	
	if (Frame_max<=9) then
		write(number1,'(I1)')Frame_max
	elseif ( Frame_max>=10 .AND. Frame_max<=99) then
		write(number2,'(I2)')Frame_max
	elseif ( Frame_max>=100 .AND. Frame_max<=999) then
		write(number3,'(I3)')Frame_max
	elseif ( Frame_max>=1000 .AND. Frame_max<=9999) then
		write(number4,'(I4)')Frame_max
	end if
	
		if (Frame_max<=9) then
		open(unit=1, file = "./GIF_temporary/"//number1, form='formatted',status='unknown',action='write')
	elseif ( Frame_max>=10 .AND. Frame_max<=99) then
		open(unit=1, file = "./GIF_temporary/"//number2, form='formatted',status='unknown',action='write')
	elseif ( Frame_max>=100 .AND. Frame_max<=999) then
		open(unit=1, file = "./GIF_temporary/"//number3, form='formatted',status='unknown',action='write')
	elseif ( Frame_max>=1000 .AND. Frame_max<=9999) then
		open(unit=1, file = "./GIF_temporary/"//number4, form='formatted',status='unknown',action='write')
	end if
	
	if (GIF_parameter==1) then
		write(1,*)0,X
		do j=0,Ny+1
			write(1,*)Y(j),mesh(:,j)%v_Pa_Pressure
		end do
	end if
	
	if (GIF_parameter==2) then
		write(1,*)0,X
		do j=0,Ny+1
			write(1,*)Y(j),mesh(:,j)%v_mps_Speed(1)
		end do
	end if
	
	if (GIF_parameter==3) then
		write(1,*)0,X
		do j=0,Ny+1
			write(1,*)Y(j),mesh(:,j)%v_mps_Speed(2)
		end do
	end if
	
	if (GIF_parameter==4) then
		write(1,*)0,X
		do j=0,Ny+1
			write(1,*)Y(j),mesh(:,j)%v_Pa_Strain(1)
		end do
	end if
	
	if (GIF_parameter==5) then
		write(1,*)0,X
		do j=0,Ny+1
			write(1,*)Y(j),mesh(:,j)%v_Pa_Strain(2)
		end do
	end if
	
	if (GIF_parameter==6) then
		write(1,*)0,X
		do j=0,Ny+1
			write(1,*)Y(j),mesh(:,j)%v_Pa_Strain(3)
		end do
	end if
	
	close(1)
	
	Frame_max=Frame_max+1

end subroutine

subroutine GIF_Animation(Frame_max,v_x_meshMinMax,Delay)
	implicit none
	integer::i
	integer,intent(in)::Frame_max,Delay
	type(cell),dimension(2),intent(in)::v_x_meshMinMax
	
	if (plot_type==1) then
		!GNUPLOT command file
		open(unit=2, file = "./GIF_temporary/command_line.txt", form='formatted',status='unknown',action='write')
		write(2,*)'set xrange[0:',(Ny+1)*dy,']'
		write(2,*)'set yrange[0:',(Nx+1)*dx,']'
	
		if (GIF_parameter==1) then
			write(2,*)'set zrange[',v_x_meshMinMax(1)%v_pa_pressure,':',v_x_meshMinMax(2)%v_pa_pressure,']'
		
		elseif (GIF_parameter==2) then
			write(2,*)'set zrange[',v_x_meshMinMax(1)%v_mps_speed(1),':',v_x_meshMinMax(2)%v_mps_speed(1),']'
		
		elseif (GIF_parameter==3) then
			write(2,*)'set zrange[',v_x_meshMinMax(1)%v_mps_speed(2),':',v_x_meshMinMax(2)%v_mps_speed(2),']'
	
		elseif (GIF_parameter==4) then
			write(2,*)'set zrange[',v_x_meshMinMax(1)%v_pa_strain(1),':',v_x_meshMinMax(2)%v_pa_strain(1),']'


		elseif (GIF_parameter==5) then
			write(2,*)'set zrange[',v_x_meshMinMax(1)%v_pa_strain(2),':',v_x_meshMinMax(2)%v_pa_strain(2),']'
	
		elseif (GIF_parameter==6) then
			write(2,*)'set zrange[',v_x_meshMinMax(1)%v_pa_strain(3),':',v_x_meshMinMax(2)%v_pa_strain(3),']'
		end if
	
		write(2,'(A31,I2)')'set terminal gif animate delay ',Delay
		write(2,*)'set output "animation.gif"'
	
		do i=0,Frame_max-1
			if (i<=9) then
				write(2,'(A7,I1,A23)')'splot "',i,'" matrix nonuniform w l'
			elseif ( i>=10 .AND. i<=99) then
			write(2,'(A7,I2,A23)')'splot "',i,'" matrix nonuniform w l'
			elseif ( i>=100 .AND. i<=999) then
				write(2,'(A7,I3,A23)')'splot "',i,'" matrix nonuniform w l'
			elseif ( i>=1000 .AND. i<=9999) then
				write(2,'(A7,I4,A23)')'splot "',i,'" matrix nonuniform w l'
			end if
		end do
		
	elseif (plot_type==2) then
		!GNUPLOT command file
		open(unit=2, file = "./GIF_temporary/command_line.txt", form='formatted',status='unknown',action='write')
		write(2,*)'set pm3d map'
		!write(2,*)'set pm3d interpolate 0,0'
		write(2,*)'set xrange[0:',(Ny+1)*dy,']'
		write(2,*)'set yrange[0:',(Nx+1)*dx,']'
	
		if (GIF_parameter==1) then
			write(2,*)'set cbrange [',v_x_meshMinMax(1)%v_pa_pressure,':',v_x_meshMinMax(2)%v_pa_pressure,']'
			write(2,*)'set palette defined (',v_x_meshMinMax(1)%v_pa_pressure,&
			&'"blue"',',',' 0',' "white", ',v_x_meshMinMax(2)%v_pa_pressure,' "red")'
		
		elseif (GIF_parameter==2) then
			write(2,*)'set cbrange [',v_x_meshMinMax(1)%v_mps_speed(1),':',v_x_meshMinMax(2)%v_mps_speed(1),']'
			write(2,*)'set palette defined (',v_x_meshMinMax(1)%v_mps_speed(1),&
			&'"blue"',',',' 0',' "white", ',v_x_meshMinMax(2)%v_pa_pressure,' "red")'
		
		elseif (GIF_parameter==3) then
			write(2,*)'set cbrange [',v_x_meshMinMax(1)%v_mps_speed(2),':',v_x_meshMinMax(2)%v_mps_speed(2),']'
			write(2,*)'set palette defined (',v_x_meshMinMax(1)%v_mps_speed(2),&
			&'"blue"',',',' 0',' "white", ',v_x_meshMinMax(2)%v_mps_speed(2),' "red")'

		elseif (GIF_parameter==4) then
			write(2,*)'set cbrange [',v_x_meshMinMax(1)%v_pa_strain(1),':',v_x_meshMinMax(2)%v_pa_strain(1),']'
			write(2,*)'set palette defined (',v_x_meshMinMax(1)%v_pa_strain(1),&
			&'"blue"',',',' 0',' "white", ',v_x_meshMinMax(2)%v_pa_strain(1),' "red")'

		elseif (GIF_parameter==5) then
			write(2,*)'set cbrange [',v_x_meshMinMax(1)%v_pa_strain(2),':',v_x_meshMinMax(2)%v_pa_strain(2),']'
			write(2,*)'set palette defined (',v_x_meshMinMax(1)%v_pa_strain(2),&
			&'"blue"',',',' 0',' "white", ',v_x_meshMinMax(2)%v_pa_strain(2),' "red")'
	
		elseif (GIF_parameter==6) then
			write(2,*)'set cbrange [',v_x_meshMinMax(1)%v_pa_strain(3),':',v_x_meshMinMax(2)%v_pa_strain(3),']'
			write(2,*)'set palette defined (',v_x_meshMinMax(1)%v_pa_strain(3),&
			&'"blue"',',',' 0',' "white", ',v_x_meshMinMax(2)%v_pa_strain(3),' "red")'
		end if
	
		write(2,'(A31,I2)')'set terminal gif animate delay ',Delay
		write(2,*)'set output "animation.gif"'
	
		do i=0,Frame_max-1
			if (i<=9) then
				write(2,'(A7,I1,A19)')'splot "',i,'" matrix nonuniform'
			elseif ( i>=10 .AND. i<=99) then
			write(2,'(A7,I2,A19)')'splot "',i,'" matrix nonuniform'
			elseif ( i>=100 .AND. i<=999) then
				write(2,'(A7,I3,A19)')'splot "',i,'" matrix nonuniform'
			elseif ( i>=1000 .AND. i<=9999) then
				write(2,'(A7,I4,A19)')'splot "',i,'" matrix nonuniform'
			end if
		end do
		end if
	close(2)
	
	CALL System('cd ./GIF_temporary ; gnuplot command_line.txt')
	CALL System('cp ./GIF_temporary/animation.gif .') 
	
end subroutine

!seek_MinMax get the min (1) and the max (2) of each value on the mesh grid
Subroutine seek_MinMax(v_x_meshMinMax)
	implicit none
	integer::i,j
	type(cell),dimension(2),intent(inout)::v_x_meshMinMax
	do j=0,Ny+1
		do i=0,Nx+1
			v_x_meshMinMax(1)%v_mps_speed(1)=min(v_x_meshMinMax(1)%v_mps_speed(1),mesh(i,j)%v_mps_speed(1))
			v_x_meshMinMax(1)%v_mps_speed(2)=min(v_x_meshMinMax(1)%v_mps_speed(2),mesh(i,j)%v_mps_speed(2))
			v_x_meshMinMax(2)%v_mps_speed(1)=max(v_x_meshMinMax(2)%v_mps_speed(1),mesh(i,j)%v_mps_speed(1))
			v_x_meshMinMax(2)%v_mps_speed(2)=max(v_x_meshMinMax(2)%v_mps_speed(2),mesh(i,j)%v_mps_speed(2))
		
			v_x_meshMinMax(1)%v_pa_pressure=min(v_x_meshMinMax(1)%v_pa_pressure,mesh(i,j)%v_pa_pressure)
			v_x_meshMinMax(2)%v_pa_pressure=max(v_x_meshMinMax(2)%v_pa_pressure,mesh(i,j)%v_pa_pressure)
		
			v_x_meshMinMax(1)%v_pa_strain(1)=min(v_x_meshMinMax(1)%v_pa_strain(1),mesh(i,j)%v_pa_strain(1))
			v_x_meshMinMax(1)%v_pa_strain(2)=min(v_x_meshMinMax(1)%v_pa_strain(2),mesh(i,j)%v_pa_strain(2))
			v_x_meshMinMax(1)%v_pa_strain(3)=min(v_x_meshMinMax(1)%v_pa_strain(3),mesh(i,j)%v_pa_strain(3))
			v_x_meshMinMax(2)%v_pa_strain(1)=max(v_x_meshMinMax(2)%v_pa_strain(1),mesh(i,j)%v_pa_strain(1))
			v_x_meshMinMax(2)%v_pa_strain(2)=max(v_x_meshMinMax(2)%v_pa_strain(2),mesh(i,j)%v_pa_strain(2))
			v_x_meshMinMax(2)%v_pa_strain(3)=max(v_x_meshMinMax(2)%v_pa_strain(3),mesh(i,j)%v_pa_strain(3))
		end do
	end do
end subroutine


!---------------
!NEW GIF VERSION
!---------------
!Creates GIF data
subroutine GIF_Frame2(GIF_parameter,Frame_Number,Frame_max,GIF_plot)
	implicit none
	integer::i,j
	integer,intent(in)::GIF_parameter,Frame_Number
	integer,intent(in),dimension(6)::GIF_plot
	integer,intent(inout)::Frame_max
	!Contains the values of the mesh points
	real*8,dimension(0:Nx+1)::X
	real*8,dimension(0:Ny+1)::Y
	
	!Creation of a character using on integer:
	!len=1 for the numbers [0:9]
	character(len=1)::number1
	!len=2 for the numbers [10:99]
	character(len=2)::number2
	!len=3 for the numbers [100:999]
	character(len=3)::number3
	!len=4 for the numbers [1000:9999]
	character(len=4)::number4
	
	do i=0,Nx+1
		X(i)=i*dx
	end do
	
	do i=0,Ny+1
		Y(i)=i*dy
	end do
	
	if (Frame_max<=9) then
		write(number1,'(I1)')Frame_max
	elseif ( Frame_max>=10 .AND. Frame_max<=99) then
		write(number2,'(I2)')Frame_max
	elseif ( Frame_max>=100 .AND. Frame_max<=999) then
		write(number3,'(I3)')Frame_max
	elseif ( Frame_max>=1000 .AND. Frame_max<=9999) then
		write(number4,'(I4)')Frame_max
	end if
	
	!Creation of temporary files
	if (plot_type==1 .OR. plot_type==2) then
	
		if(GIF_plot(1)==1) then
	
			if (Frame_max<=9) then
				open(unit=1, file = "./GIF_temporary1/"//number1, form='formatted',status='unknown',action='write')
			elseif ( Frame_max>=10 .AND. Frame_max<=99) then
				open(unit=1, file = "./GIF_temporary1/"//number2, form='formatted',status='unknown',action='write')
			elseif ( Frame_max>=100 .AND. Frame_max<=999) then
				open(unit=1, file = "./GIF_temporary1/"//number3, form='formatted',status='unknown',action='write')
			elseif ( Frame_max>=1000 .AND. Frame_max<=9999) then
				open(unit=1, file = "./GIF_temporary1/"//number4, form='formatted',status='unknown',action='write')
			end if
	
			write(1,*)0,X
			do j=0,Ny+1
					write(1,*)Y(j),mesh(:,j)%v_Pa_Pressure
			end do
		end if
		close(1)
		if(GIF_plot(2)==1) then
	
				if (Frame_max<=9) then
				open(unit=2, file = "./GIF_temporary2/"//number1, form='formatted',status='unknown',action='write')
			elseif ( Frame_max>=10 .AND. Frame_max<=99) then
				open(unit=2, file = "./GIF_temporary2/"//number2, form='formatted',status='unknown',action='write')
			elseif ( Frame_max>=100 .AND. Frame_max<=999) then
				open(unit=2, file = "./GIF_temporary2/"//number3, form='formatted',status='unknown',action='write')
			elseif ( Frame_max>=1000 .AND. Frame_max<=9999) then
				open(unit=2, file = "./GIF_temporary2/"//number4, form='formatted',status='unknown',action='write')
			end if
	
			write(2,*)0,X
			do j=0,Ny+1
					write(2,*)Y(j),mesh(:,j)%v_mps_Speed(1)
			end do
		end if
		close(2)
	
		if(GIF_plot(3)==1) then
			if (Frame_max<=9) then
				open(unit=3, file = "./GIF_temporary3/"//number1, form='formatted',status='unknown',action='write')
			elseif ( Frame_max>=10 .AND. Frame_max<=99) then
				open(unit=3, file = "./GIF_temporary3/"//number2, form='formatted',status='unknown',action='write')
			elseif ( Frame_max>=100 .AND. Frame_max<=999) then
				open(unit=3, file = "./GIF_temporary3/"//number3, form='formatted',status='unknown',action='write')
			elseif ( Frame_max>=1000 .AND. Frame_max<=9999) then
				open(unit=3, file = "./GIF_temporary3/"//number4, form='formatted',status='unknown',action='write')
				end if
		
			write(3,*)0,X
			do j=0,Ny+1
					write(3,*)Y(j),mesh(:,j)%v_mps_Speed(2)
			end do	
		end if
		close(3)
		
		if(GIF_plot(4)==1) then
		
				if (Frame_max<=9) then
				open(unit=4, file = "./GIF_temporary4/"//number1, form='formatted',status='unknown',action='write')
			elseif ( Frame_max>=10 .AND. Frame_max<=99) then
				open(unit=4, file = "./GIF_temporary4/"//number2, form='formatted',status='unknown',action='write')
			elseif ( Frame_max>=100 .AND. Frame_max<=999) then
				open(unit=4, file = "./GIF_temporary4/"//number3, form='formatted',status='unknown',action='write')
			elseif ( Frame_max>=1000 .AND. Frame_max<=9999) then
				open(unit=4, file = "./GIF_temporary4/"//number4, form='formatted',status='unknown',action='write')
			end if
		
			write(4,*)0,X
			do j=0,Ny+1
					write(4,*)Y(j),mesh(:,j)%v_Pa_Strain(1)
			end do
		end if
		close(4)
		
		if(GIF_plot(5)==1) then
		
			if (Frame_max<=9) then
				open(unit=5, file = "./GIF_temporary5/"//number1, form='formatted',status='unknown',action='write')
			elseif ( Frame_max>=10 .AND. Frame_max<=99) then
				open(unit=5, file = "./GIF_temporary5/"//number2, form='formatted',status='unknown',action='write')
			elseif ( Frame_max>=100 .AND. Frame_max<=999) then
				open(unit=5, file = "./GIF_temporary5/"//number3, form='formatted',status='unknown',action='write')
			elseif ( Frame_max>=1000 .AND. Frame_max<=9999) then
				open(unit=5, file = "./GIF_temporary5/"//number4, form='formatted',status='unknown',action='write')
			end if
		
			write(5,*)0,X
			do j=0,Ny+1
					write(5,*)Y(j),mesh(:,j)%v_Pa_Strain(2)
			end do	
		end if
		close(5)
		
		if(GIF_plot(6)==1) then
		
			if (Frame_max<=9) then
				open(unit=7, file = "./GIF_temporary6/"//number1, form='formatted',status='unknown',action='write')
			elseif ( Frame_max>=10 .AND. Frame_max<=99) then
				open(unit=7, file = "./GIF_temporary6/"//number2, form='formatted',status='unknown',action='write')
			elseif ( Frame_max>=100 .AND. Frame_max<=999) then
				open(unit=7, file = "./GIF_temporary6/"//number3, form='formatted',status='unknown',action='write')
			elseif ( Frame_max>=1000 .AND. Frame_max<=9999) then
				open(unit=7, file = "./GIF_temporary6/"//number4, form='formatted',status='unknown',action='write')
			end if
		
			write(7,*)0,X
			do j=0,Ny+1
					write(7,*)Y(j),mesh(:,j)%v_Pa_Strain(3)
			end do
		close(7)	
		end if
	else if (plot_type==3) then
	
			if (Frame_max<=9) then
				open(unit=1, file = "./GIF_temporary1/"//number1, form='formatted',status='unknown',action='write')
			elseif ( Frame_max>=10 .AND. Frame_max<=99) then
				open(unit=1, file = "./GIF_temporary1/"//number2, form='formatted',status='unknown',action='write')
			elseif ( Frame_max>=100 .AND. Frame_max<=999) then
				open(unit=1, file = "./GIF_temporary1/"//number3, form='formatted',status='unknown',action='write')
			elseif ( Frame_max>=1000 .AND. Frame_max<=9999) then
				open(unit=1, file = "./GIF_temporary1/"//number4, form='formatted',status='unknown',action='write')
			end if

			do i=0,Nx+1
				do j=0,Ny+1
						write(1,*)X(i),Y(j),mesh(i,j)%v_mps_Speed(1)*mesh(i,j)%m_mask,mesh(i,j)%v_mps_Speed(2)*mesh(i,j)%m_mask
				end do
			end do
	
			close(1)
	end if
	
	Frame_max=Frame_max+1

end subroutine



subroutine GIF_Animation2(Frame_max,v_x_meshMinMax,Delay,GIF_plot)
	implicit none
	integer::i
	integer,intent(in)::Frame_max,Delay
	integer,intent(in),dimension(6)::GIF_plot
	type(cell),dimension(2),intent(in)::v_x_meshMinMax
	
	if (plot_type==1) then
	
		if(GIF_plot(1)==1) then
			!GNUPLOT command file
			open(unit=40, file = "./GIF_temporary1/command_line.txt", form='formatted',status='unknown',action='write')
			write(40,*)'set xrange[0:',(Ny+1)*dy,']'
			write(40,*)'set yrange[0:',(Nx+1)*dx,']'
			write(40,*)'set zrange[',v_x_meshMinMax(1)%v_pa_pressure,':',v_x_meshMinMax(2)%v_pa_pressure,']'
			write(40,'(A31,I2)')'set terminal gif animate delay ',Delay
			write(40,*)'set output "pressure.gif"'
		do i=0,Frame_max-1
			if (i<=9) then
				write(40,'(A7,I1,A23)')'splot "',i,'" matrix nonuniform w l'
			elseif ( i>=10 .AND. i<=99) then
			write(40,'(A7,I2,A23)')'splot "',i,'" matrix nonuniform w l'
			elseif ( i>=100 .AND. i<=999) then
				write(40,'(A7,I3,A23)')'splot "',i,'" matrix nonuniform w l'
			elseif ( i>=1000 .AND. i<=9999) then
				write(40,'(A7,I4,A23)')'splot "',i,'" matrix nonuniform w l'
			end if
		end do
		close(40)
		CALL System('cd ./GIF_temporary1 ; gnuplot command_line.txt')
		CALL System('cp ./GIF_temporary1/pressure.gif .') 
		CALL System('rm -r GIF_temporary1')
		end if
		
		if(GIF_plot(2)==1) then
			!GNUPLOT command file
			open(unit=41, file = "./GIF_temporary2/command_line.txt", form='formatted',status='unknown',action='write')
			write(41,*)'set xrange[0:',(Ny+1)*dy,']'
			write(41,*)'set yrange[0:',(Nx+1)*dx,']'
			write(41,*)'set zrange[',v_x_meshMinMax(1)%v_mps_speed(1),':',v_x_meshMinMax(2)%v_mps_speed(1),']'			
			write(41,'(A31,I2)')'set terminal gif animate delay ',Delay
			write(41,*)'set output "u_velocity.gif"'
		do i=0,Frame_max-1
			if (i<=9) then
				write(41,'(A7,I1,A23)')'splot "',i,'" matrix nonuniform w l'
			elseif ( i>=10 .AND. i<=99) then
			write(41,'(A7,I2,A23)')'splot "',i,'" matrix nonuniform w l'
			elseif ( i>=100 .AND. i<=999) then
				write(41,'(A7,I3,A23)')'splot "',i,'" matrix nonuniform w l'
			elseif ( i>=1000 .AND. i<=9999) then
				write(41,'(A7,I4,A23)')'splot "',i,'" matrix nonuniform w l'
			end if
		end do
		close(41)
		CALL System('cd ./GIF_temporary2 ; gnuplot command_line.txt')
		CALL System('cp ./GIF_temporary2/u_velocity.gif .') 
		CALL System('rm -r GIF_temporary2')
		end if
		
		if(GIF_plot(3)==1) then
			!GNUPLOT command file
			open(unit=42, file = "./GIF_temporary3/command_line.txt", form='formatted',status='unknown',action='write')
			write(42,*)'set xrange[0:',(Ny+1)*dy,']'
			write(42,*)'set yrange[0:',(Nx+1)*dx,']'
			write(42,*)'set zrange[',v_x_meshMinMax(1)%v_mps_speed(2),':',v_x_meshMinMax(2)%v_mps_speed(2),']'
			write(42,'(A31,I2)')'set terminal gif animate delay ',Delay
			write(42,*)'set output "v_velocity.gif"'
		do i=0,Frame_max-1
			if (i<=9) then
				write(42,'(A7,I1,A23)')'splot "',i,'" matrix nonuniform w l'
			elseif ( i>=10 .AND. i<=99) then
			write(42,'(A7,I2,A23)')'splot "',i,'" matrix nonuniform w l'
			elseif ( i>=100 .AND. i<=999) then
				write(42,'(A7,I3,A23)')'splot "',i,'" matrix nonuniform w l'
			elseif ( i>=1000 .AND. i<=9999) then
				write(42,'(A7,I4,A23)')'splot "',i,'" matrix nonuniform w l'
			end if
		end do
		close(42)
		CALL System('cd ./GIF_temporary3 ; gnuplot command_line.txt')
		CALL System('cp ./GIF_temporary3/v_velocity.gif .') 
		CALL System('rm -r GIF_temporary3')
		end if
		
		if(GIF_plot(4)==1) then
			!GNUPLOT command file
			open(unit=43, file = "./GIF_temporary4/command_line.txt", form='formatted',status='unknown',action='write')
			write(43,*)'set xrange[0:',(Ny+1)*dy,']'
			write(43,*)'set yrange[0:',(Nx+1)*dx,']'
			write(43,*)'set zrange[',v_x_meshMinMax(1)%v_pa_strain(1),':',v_x_meshMinMax(2)%v_pa_strain(1),']'
			write(43,'(A31,I2)')'set terminal gif animate delay ',Delay
			write(43,*)'set output "Strain_1.gif"'	
		do i=0,Frame_max-1
			if (i<=9) then
				write(43,'(A7,I1,A23)')'splot "',i,'" matrix nonuniform w l'
			elseif ( i>=10 .AND. i<=99) then
			write(43,'(A7,I2,A23)')'splot "',i,'" matrix nonuniform w l'
			elseif ( i>=100 .AND. i<=999) then
				write(43,'(A7,I3,A23)')'splot "',i,'" matrix nonuniform w l'
			elseif ( i>=1000 .AND. i<=9999) then
				write(43,'(A7,I4,A23)')'splot "',i,'" matrix nonuniform w l'
			end if
		end do
		close(43)
		CALL System('cd ./GIF_temporary4 ; gnuplot command_line.txt')
		CALL System('cp ./GIF_temporary4/Strain_1.gif .') 
		CALL System('rm -r GIF_temporary4')		
		end if
		
		if(GIF_plot(5)==1) then
			open(unit=44, file = "./GIF_temporary5/command_line.txt", form='formatted',status='unknown',action='write')
			write(44,*)'set xrange[0:',(Ny+1)*dy,']'
			write(44,*)'set yrange[0:',(Nx+1)*dx,']'
			write(44,*)'set zrange[',v_x_meshMinMax(1)%v_pa_strain(2),':',v_x_meshMinMax(2)%v_pa_strain(2),']'
			write(44,'(A31,I2)')'set terminal gif animate delay ',Delay
			write(44,*)'set output "Strain_2.gif"'
		do i=0,Frame_max-1
			if (i<=9) then
				write(44,'(A7,I1,A23)')'splot "',i,'" matrix nonuniform w l'
			elseif ( i>=10 .AND. i<=99) then
			write(44,'(A7,I2,A23)')'splot "',i,'" matrix nonuniform w l'
			elseif ( i>=100 .AND. i<=999) then
				write(44,'(A7,I3,A23)')'splot "',i,'" matrix nonuniform w l'
			elseif ( i>=1000 .AND. i<=9999) then
				write(44,'(A7,I4,A23)')'splot "',i,'" matrix nonuniform w l'
			end if
		end do
		close(44)
		CALL System('cd ./GIF_temporary5 ; gnuplot command_line.txt')
		CALL System('cp ./GIF_temporary5/Strain_2.gif .') 
		CALL System('rm -r GIF_temporary5')
		end if
		
		if(GIF_plot(6)==1) then
			open(unit=45, file = "./GIF_temporary6/command_line.txt", form='formatted',status='unknown',action='write')
			write(45,*)'set xrange[0:',(Ny+1)*dy,']'
			write(45,*)'set yrange[0:',(Nx+1)*dx,']'
			write(45,*)'set zrange[',v_x_meshMinMax(1)%v_pa_strain(3),':',v_x_meshMinMax(2)%v_pa_strain(3),']'
			write(45,'(A31,I2)')'set terminal gif animate delay ',Delay
			write(45,*)'set output "Strain_3.gif"'
		do i=0,Frame_max-1
			if (i<=9) then
				write(45,'(A7,I1,A23)')'splot "',i,'" matrix nonuniform w l'
			elseif ( i>=10 .AND. i<=99) then
			write(45,'(A7,I2,A23)')'splot "',i,'" matrix nonuniform w l'
			elseif ( i>=100 .AND. i<=999) then
				write(45,'(A7,I3,A23)')'splot "',i,'" matrix nonuniform w l'
			elseif ( i>=1000 .AND. i<=9999) then
				write(45,'(A7,I4,A23)')'splot "',i,'" matrix nonuniform w l'
			end if
		end do
		close(45)
		CALL System('cd ./GIF_temporary6 ; gnuplot command_line.txt')
		CALL System('cp ./GIF_temporary6/Strain_3.gif .') 
		CALL System('rm -r GIF_temporary6')
		end if
	
		
	elseif (plot_type==2) then
		
		if(GIF_plot(1)==1) then
			!GNUPLOT command file
			open(unit=40, file = "./GIF_temporary1/command_line.txt", form='formatted',status='unknown',action='write')
			write(40,*)'set pm3d map'
			write(40,*)'set xrange[0:',(Ny+1)*dy,']'
			write(40,*)'set yrange[0:',(Nx+1)*dx,']'
			write(40,*)'set cbrange [',v_x_meshMinMax(1)%v_pa_pressure,':',v_x_meshMinMax(2)%v_pa_pressure,']'
			write(40,*)'set palette defined (',v_x_meshMinMax(1)%v_pa_pressure,&
			&'"blue"',',',' 0',' "white", ',v_x_meshMinMax(2)%v_pa_pressure,' "red")'
			write(40,'(A31,I2)')'set terminal gif animate delay ',Delay
			write(40,*)'set output "pressure.gif"'
		do i=0,Frame_max-1
			if (i<=9) then
				write(40,'(A7,I1,A19)')'splot "',i,'" matrix nonuniform'
			elseif ( i>=10 .AND. i<=99) then
				write(40,'(A7,I2,A19)')'splot "',i,'" matrix nonuniform'
			elseif ( i>=100 .AND. i<=999) then
				write(40,'(A7,I3,A19)')'splot "',i,'" matrix nonuniform'
			elseif ( i>=1000 .AND. i<=9999) then
				write(40,'(A7,I4,A19)')'splot "',i,'" matrix nonuniform'
			end if
		end do
		close(40)
		CALL System('cd ./GIF_temporary1 ; gnuplot command_line.txt')
		CALL System('cp ./GIF_temporary1/pressure.gif .') 
		CALL System('rm -r GIF_temporary1')
		end if
		
		if(GIF_plot(2)==1) then
			!GNUPLOT command file
			open(unit=41, file = "./GIF_temporary2/command_line.txt", form='formatted',status='unknown',action='write')
			write(41,*)'set pm3d map'
			write(41,*)'set xrange[0:',(Ny+1)*dy,']'
			write(41,*)'set yrange[0:',(Nx+1)*dx,']'
			write(41,*)'set cbrange [',v_x_meshMinMax(1)%v_mps_speed(1),':',v_x_meshMinMax(2)%v_mps_speed(1),']'
			write(41,*)'set palette defined (',v_x_meshMinMax(1)%v_mps_speed(1),&
			&'"blue"',',',' 0',' "white", ',v_x_meshMinMax(2)%v_pa_pressure,' "red")'
			write(41,'(A31,I2)')'set terminal gif animate delay ',Delay
			write(41,*)'set output "u_velocity.gif"'
		do i=0,Frame_max-1
			if (i<=9) then
				write(41,'(A7,I1,A19)')'splot "',i,'" matrix nonuniform'
			elseif ( i>=10 .AND. i<=99) then
				write(41,'(A7,I2,A19)')'splot "',i,'" matrix nonuniform'
			elseif ( i>=100 .AND. i<=999) then
				write(41,'(A7,I3,A19)')'splot "',i,'" matrix nonuniform'
			elseif ( i>=1000 .AND. i<=9999) then
				write(41,'(A7,I4,A19)')'splot "',i,'" matrix nonuniform'
			end if
		end do
		close(41)
		CALL System('cd ./GIF_temporary2 ; gnuplot command_line.txt')
		CALL System('cp ./GIF_temporary2/u_velocity.gif .') 
		CALL System('rm -r GIF_temporary2')
		end if
		
		if(GIF_plot(3)==1) then
			!GNUPLOT command file
			open(unit=42, file = "./GIF_temporary3/command_line.txt", form='formatted',status='unknown',action='write')
			write(42,*)'set pm3d map'
			write(42,*)'set xrange[0:',(Ny+1)*dy,']'
			write(42,*)'set yrange[0:',(Nx+1)*dx,']'
			write(42,*)'set cbrange [',v_x_meshMinMax(1)%v_mps_speed(2),':',v_x_meshMinMax(2)%v_mps_speed(2),']'
			write(42,*)'set palette defined (',v_x_meshMinMax(1)%v_mps_speed(2),&
			&'"blue"',',',' 0',' "white", ',v_x_meshMinMax(2)%v_mps_speed(2),' "red")'
			write(42,'(A31,I2)')'set terminal gif animate delay ',Delay
			write(42,*)'set output "v_velocity.gif"'
		do i=0,Frame_max-1
			if (i<=9) then
				write(42,'(A7,I1,A19)')'splot "',i,'" matrix nonuniform'
			elseif ( i>=10 .AND. i<=99) then
				write(42,'(A7,I2,A19)')'splot "',i,'" matrix nonuniform'
			elseif ( i>=100 .AND. i<=999) then
				write(42,'(A7,I3,A19)')'splot "',i,'" matrix nonuniform'
			elseif ( i>=1000 .AND. i<=9999) then
				write(42,'(A7,I4,A19)')'splot "',i,'" matrix nonuniform'
			end if
		end do
		close(42)
		CALL System('cd ./GIF_temporary3 ; gnuplot command_line.txt')
		CALL System('cp ./GIF_temporary3/v_velocity.gif .') 
		CALL System('rm -r GIF_temporary3')
		end if
		
		if(GIF_plot(4)==1) then
			!GNUPLOT command file
			open(unit=43, file = "./GIF_temporary4/command_line.txt", form='formatted',status='unknown',action='write')
			write(43,*)'set pm3d map'
			write(43,*)'set xrange[0:',(Ny+1)*dy,']'
			write(43,*)'set yrange[0:',(Nx+1)*dx,']'
			write(43,*)'set cbrange [',v_x_meshMinMax(1)%v_pa_strain(1),':',v_x_meshMinMax(2)%v_pa_strain(1),']'
			write(43,*)'set palette defined (',v_x_meshMinMax(1)%v_pa_strain(1),&
			&'"blue"',',',' 0',' "white", ',v_x_meshMinMax(2)%v_pa_strain(1),' "red")'
			write(43,'(A31,I2)')'set terminal gif animate delay ',Delay
			write(43,*)'set output "Strain_1.gif"'
		do i=0,Frame_max-1
			if (i<=9) then
				write(43,'(A7,I1,A19)')'splot "',i,'" matrix nonuniform'
			elseif ( i>=10 .AND. i<=99) then
				write(43,'(A7,I2,A19)')'splot "',i,'" matrix nonuniform'
			elseif ( i>=100 .AND. i<=999) then
				write(43,'(A7,I3,A19)')'splot "',i,'" matrix nonuniform'
			elseif ( i>=1000 .AND. i<=9999) then
				write(43,'(A7,I4,A19)')'splot "',i,'" matrix nonuniform'
			end if
		end do
		close(43)
		CALL System('cd ./GIF_temporary4 ; gnuplot command_line.txt')
		CALL System('cp ./GIF_temporary4/Strain_1.gif .') 
		CALL System('rm -r GIF_temporary4')
		end if
		
		if(GIF_plot(5)==1) then
			!GNUPLOT command file
			open(unit=44, file = "./GIF_temporary5/command_line.txt", form='formatted',status='unknown',action='write')
			write(44,*)'set pm3d map'
			write(44,*)'set xrange[0:',(Ny+1)*dy,']'
			write(44,*)'set yrange[0:',(Nx+1)*dx,']'
			write(44,*)'set cbrange [',v_x_meshMinMax(1)%v_pa_strain(2),':',v_x_meshMinMax(2)%v_pa_strain(2),']'
			write(44,*)'set palette defined (',v_x_meshMinMax(1)%v_pa_strain(2),&
			&'"blue"',',',' 0',' "white", ',v_x_meshMinMax(2)%v_pa_strain(2),' "red")'
			write(44,'(A31,I2)')'set terminal gif animate delay ',Delay
			write(44,*)'set output "Strain_2.gif"'
		do i=0,Frame_max-1
			if (i<=9) then
				write(44,'(A7,I1,A19)')'splot "',i,'" matrix nonuniform'
			elseif ( i>=10 .AND. i<=99) then
				write(44,'(A7,I2,A19)')'splot "',i,'" matrix nonuniform'
			elseif ( i>=100 .AND. i<=999) then
				write(44,'(A7,I3,A19)')'splot "',i,'" matrix nonuniform'
			elseif ( i>=1000 .AND. i<=9999) then
				write(44,'(A7,I4,A19)')'splot "',i,'" matrix nonuniform'
			end if
		end do
		close(44)
		CALL System('cd ./GIF_temporary5 ; gnuplot command_line.txt')
		CALL System('cp ./GIF_temporary5/Strain_2.gif .') 
		CALL System('rm -r GIF_temporary5')
		end if
		
		if(GIF_plot(6)==1) then
			!GNUPLOT command file
			open(unit=45, file = "./GIF_temporary6/command_line.txt", form='formatted',status='unknown',action='write')
			write(45,*)'set pm3d map'
			write(45,*)'set xrange[0:',(Ny+1)*dy,']'
			write(45,*)'set yrange[0:',(Nx+1)*dx,']'
			write(45,*)'set cbrange [',v_x_meshMinMax(1)%v_pa_strain(3),':',v_x_meshMinMax(2)%v_pa_strain(3),']'
			write(45,*)'set palette defined (',v_x_meshMinMax(1)%v_pa_strain(3),&
			&'"blue"',',',' 0',' "white", ',v_x_meshMinMax(2)%v_pa_strain(3),' "red")'
			write(45,'(A31,I2)')'set terminal gif animate delay ',Delay
			write(45,*)'set output "Strain_3.gif"'
		do i=0,Frame_max-1
			if (i<=9) then
				write(45,'(A7,I1,A19)')'splot "',i,'" matrix nonuniform'
			elseif ( i>=10 .AND. i<=99) then
				write(45,'(A7,I2,A19)')'splot "',i,'" matrix nonuniform'
			elseif ( i>=100 .AND. i<=999) then
				write(45,'(A7,I3,A19)')'splot "',i,'" matrix nonuniform'
			elseif ( i>=1000 .AND. i<=9999) then
				write(45,'(A7,I4,A19)')'splot "',i,'" matrix nonuniform'
			end if
		end do
		close(45)
		CALL System('cd ./GIF_temporary6 ; gnuplot command_line.txt')
		CALL System('cp ./GIF_temporary6/Strain_3.gif .') 
		CALL System('rm -r GIF_temporary6')
		end if
		
	else if(plot_type==3) then
			!GNUPLOT command file
			open(unit=40, file = "./GIF_temporary1/command_line.txt", form='formatted',status='unknown',action='write')
			write(40,*)'set xrange[0:',(Ny+1)*dy,']'
			write(40,*)'set yrange[0:',(Nx+1)*dx,']'
			write(40,'(A31,I2)')'set terminal gif animate delay ',Delay
			write(40,*)'set output "velocity.gif"'
		do i=0,Frame_max-1
			if (i<=9) then
				write(40,'(A7,I1,A13,F8.6,A25,F7.6,A61)')'plot "',i,'" using 1:2:(',dx,'*$3/($3**2+$4**2)**0.5):('&
				&,dy,'*$4/($3**2+$4**2)**0.5):($3**2+$4**2) with vectors lc palette'
			elseif ( i>=10 .AND. i<=99) then
				write(40,'(A7,I2,A13,F8.6,A25,F7.6,A61)')'plot "',i,'" using 1:2:(',dx,'*$3/($3**2+$4**2)**0.5):('&
				&,dy,'*$4/($3**2+$4**2)**0.5):($3**2+$4**2) with vectors lc palette'
			elseif ( i>=100 .AND. i<=999) then
				write(40,'(A7,I3,A13,F8.6,A25,F7.6,A61)')'plot "',i,'" using 1:2:(',dx,'*$3/($3**2+$4**2)**0.5):('&
				&,dy,'*$4/($3**2+$4**2)**0.5):($3**2+$4**2) with vectors lc palette'
			elseif ( i>=1000 .AND. i<=9999) then
				write(40,'(A7,I4,A13,F8.6,A25,F7.6,A61)')'plot "',i,'" using 1:2:(',dx,'*$3/($3**2+$4**2)**0.5):('&
				&,dy,'*$4/($3**2+$4**2)**0.5):($3**2+$4**2) with vectors lc palette'
			end if
		if(dx<1D-6) then
			print*,'WARNING dx too small for the GIF'
		end if
		if(dy<1D-6) then
			print*,'WARNING dy too small for the GIF'
		end if
		end do
		close(40)
		CALL System('cd ./GIF_temporary1 ; gnuplot command_line.txt')
		CALL System('cp ./GIF_temporary1/velocity.gif .') 
		!CALL System('rm -r GIF_temporary1')	
	end if

	
end subroutine


end module
