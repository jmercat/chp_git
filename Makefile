all:
	gfortran -o NS.exe module_Variables.f90 module_Shared.f90 module_Init.f90 module_Step1.f90 module_Step2.f90 module_Lagrangien.f90 module_Step3.f90 module_Gif.f90 module_Mask.f90 main.f90
clean:
	rm *.mod *.o *~
