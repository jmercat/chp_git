program test

use variables
use solver
use mask
use Lagrangien
use Init


implicit none
real*8, dimension(7) :: k
real*8, dimension(:), pointer :: b, x0, x1, pd1, Ax0, x, y
integer :: i, nt, iter1, n, j, k1
real*8, dimension(:, :), pointer :: u, v
real*8 :: beta


call Init_parameters()
allocate(mesh(0:Nx+1,0:Ny+1), x(nx), y(ny))
call Make_mask(Nx, Ny, dx, dy, mesh)
print*, mesh%m_mask
call Initial_conditions(mesh)

!initialisation des valeurs de k
beta = p_pas_Viscosity+p_pas_ElasticViscosity*dt/p_s_Tau
k(1)=-2*beta*(dy/dx+dx/dy)-2*p_pas_r*dy/dx
k(2)=(beta+p_pas_r)*dy/dx
k(3)=beta*dx/dy
k(4)=-p_pas_r
k(5)=-2*beta*(dy/dx+dx/dy)-2*p_pas_r*dx/dy
k(6)=beta*dy/dx
k(7)=(beta+p_pas_r)*dx/dy

k=1000*k



nt=2*(Nx*Ny)
n=nx
allocate(b(nt), x0(nt), pd1(nt), Ax0(nt), x(n), y(n))
allocate(u(n, n), v(n, n))
x0=1
do k1=1, nt
	if (k1<=nt/2) then
	i=ceiling(k1*1.0/n*1.0)
	j=k1-(i-1)*n
	
	b(k1)=Get_Second_Membre_X_maille(mesh,i,j)
	!print*, k1, i, j,b(k1)
	elseif (k1 >nt/2) then
	i=ceiling((k1*1.0-Nx*Ny)/n*1.0) 
	j=(k1-Nx*Ny)-(i-1)*n 
	b(k1)=Get_Second_Membre_Y_maille(mesh,i,j) 
	endif
!print*,b(k1)
enddo


b=1000*b

Ax0=Aproduct2(k, x0)




call bicgstab(k, b, x0,  x1, iter1)
do i=1, n
	u(i, :)=x1((i-1)*n+1:i*n)
	v(i, :)=x1(n**2+(i-1)*n+1 : n**2+i*n)
enddo
	
	

pd1=Aproduct2(k, x1)


print*, 'convergence1 apres',iter1, 'iterations.'

do i=1, nx
	x(i)=(i-1)*dx
enddo
!print*, x

do j=1, ny
	y(j)=(j-1)*dy
enddo

call Make_mask(Nx, Ny, dx, dy, mesh)
open(unit = 12, file = 'prodmat',  status = 'unknown', action = 'write')

do i=1, nt
write(12, *) x1(i), pd1(i)
enddo
close(12)

open(unit = 24, file = 'vitessehor',  status = 'unknown', action = 'write')
do j=1,Ny
do i=1,Nx
write(24, *) x(i),y(j), u(i,j)
enddo
enddo
close(24)

	
end program
