module Init
use variables

contains
!-----------------------------------------------------------------------------------
!***********************************************************
!                 Function Init_parameters
!***********************************************************
!Initialize values
subroutine Init_parameters()
implicit none
	p_s_Tmax=0.5!s
	dt_min=0.001
	dt_max=1
	Nx=50
	Ny=50
	dx=0.02
	dy=0.02
	p_s_Tau=1
	
	!-------------
	!If the viscosity is divided by 10, 
	!the velocity must be divided by 10
	!and thus, the final time-step multiplied by 10
	p_pas_Viscosity=0.001
	p_pas_ElasticViscosity=0.001
	!-------------
	
	v_s_time=0
	v_x_meshMinMax(1)%v_mps_speed(:)=0
	v_x_meshMinMax(2)%v_mps_speed(:)=0
	v_x_meshMinMax(:)%v_pa_pressure=0
	v_x_meshMinMax(1)%v_pa_strain(:)=0
	v_x_meshMinMax(2)%v_pa_strain(:)=0
	
	!POTARS
	ToL=6D-6
	p_ppaps_eps=1D-6
	p_pas_r=1D-4
	Prec_solveur=1D-4

end subroutine
!-----------------------------------------------------------------------------------

!***********************************************************
!                 Function Initial_conditions
!***********************************************************
!Initialize mesh with initial conditions
subroutine Initial_conditions(mesh)
implicit none
	type(cell),dimension(:,:),allocatable,intent(inout)::mesh
	integer::i,j
	real*8::b
	!Parameter b for speed entry
	b=1./(Ny*dy/2)**2
	!******************
	!Inside Field cells
	!******************
	do j=1,Ny
		do i=1,Nx
			mesh(i,j)%v_mps_speed(1)=0!b*(j-0.5)*dy*((Ny-j+0.5)*dy)*(mesh(i,j)%m_mask)*(mesh(i-1,j)%m_mask)/p_pas_Viscosity
			mesh(i,j)%v_mps_speed(2)=0
			mesh(i,j)%v_pa_pressure=0
			mesh(i,j)%v_pa_strain(:)=0
		end do
	end do
	
	!----------------------------------------
	!Ouside field cells
	!----------------------------------------
	
	!***********
	!Left border
	!***********
	do j=1,Ny
		!Speed entry conditions
		mesh(0,j)%v_mps_speed(1)=b*(j-0.5)*dy*((Ny-j+0.5)*dy)
		mesh(0,j)%v_mps_speed(2)=0
		mesh(0,j)%v_pa_pressure=0
	end do
	
	!**********
	!Top border
	!**********
	do i=1,Nx
		mesh(i,Ny+1)%v_mps_speed(1)=-mesh(i,Ny)%v_mps_speed(1)
		mesh(i,Ny+1)%v_mps_speed(2)=0*(mesh(i,j)%m_mask)*(mesh(i,j-1)%m_mask)
		mesh(i,Ny+1)%v_pa_pressure=0
		mesh(i,Ny+1)%v_pa_strain(:)=0
	end do
	
	!************
	!Right border
	!************
	do j=1,Ny
		mesh(Nx+1,j)%v_mps_speed(1)=mesh(Nx,j)%v_mps_speed(1)
		mesh(Nx+1,j)%v_mps_speed(2)=mesh(Nx,j)%v_mps_speed(2)
		mesh(Nx+1,j)%v_pa_pressure=mesh(Nx,j)%v_pa_pressure
		mesh(Nx+1,j)%v_pa_strain(:)=mesh(Nx,j)%v_pa_strain(:)
	end do
	
	!*************
	!Bottom border
	!*************
	do i=1,Nx
		mesh(i,0)%v_mps_speed(1)=-mesh(i,1)%v_mps_speed(1)
		mesh(i,0)%v_mps_speed(2)=0
		mesh(i,0)%v_pa_pressure=0
		mesh(i,0)%v_pa_strain(:)=0
	end do
	
	!******************
	!Bottom left corner
	!******************
	mesh(0,0)%v_mps_speed(1)=-mesh(0,1)%v_mps_speed(1)
	mesh(0,0)%v_mps_speed(2)=0
	mesh(0,0)%v_pa_pressure=0
	mesh(0,0)%v_pa_strain(:)=0
	
	!******************
	!Bottom right corner
	!******************
	mesh(Nx+1,0)%v_mps_speed(1)=-mesh(Nx+1,1)%v_mps_speed(1)
	mesh(Nx+1,0)%v_mps_speed(2)=0
	mesh(Nx+1,0)%v_pa_pressure=0
	mesh(Nx+1,0)%v_pa_strain(:)=0
	
	!***************
	!Top left border
	!***************
	mesh(0,Ny+1)%v_mps_speed(1)=-mesh(0,Ny)%v_mps_speed(1)
	mesh(0,Ny+1)%v_mps_speed(2)=0
	mesh(0,Ny+1)%v_pa_pressure=0
	mesh(0,Ny+1)%v_pa_strain(:)=0
	
	!***************
	!Top right border
	!***************
	mesh(Nx+1,Ny+1)%v_mps_speed(1)=-mesh(Nx,Ny)%v_mps_speed(1)
	mesh(Nx+1,Ny+1)%v_mps_speed(2)=0
	mesh(Nx+1,Ny+1)%v_pa_pressure=0
	mesh(Nx+1,Ny+1)%v_pa_strain(:)=0
	
end subroutine
!-----------------------------------------------------------------------------------

end module
