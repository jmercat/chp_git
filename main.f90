!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!                       Main program used to solve NS                  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
program NS_Code

	use Init
	use Step1
	use Step2
	use Lagrangien
	use Step3
	use Gif
	use Mask
	!use lagrangien
	
	implicit none 
	!-----------------------
	!Variable initialisation
	!-----------------------
	integer::i
	real:: t1,t2
	call Init_parameters()
	allocate(mesh(0:Nx+1,0:Ny+1))
	
	!-------------------------
	!    Initial conditions
	!-------------------------
	mesh(:, :)%m_mask=1
	call Make_mask(Nx, Ny, dx, dy, mesh)
	!open(unit=12, file='masque', status='unknown')
	!do i=1, Nx
		!write(12, *) mesh(i, :)%m_mask
	!enddo
	!close(12)

	call Initial_conditions(mesh)
	
	call CPU_TIME(t1)

  call GIF_Settings(GIF_parameter,Frame_Number,Frame_max,GIF_plot)

	Frame_max=0
	 do while(v_s_time<p_s_Tmax)
		!print*,v_s_time
		if (v_s_time >= p_s_Tmax*1./Frame_number * Frame_max) then
			!Creat a Frame data and set Fram_max=Fram_max+1
			!call GIF_Frame(GIF_parameter,Frame_Number,Frame_max)
			call GIF_Frame2(GIF_parameter,Frame_Number,Frame_max,GIF_plot)
		end if

		!Calculates the CFL dt
		call Find_TimeStep(mesh,dt)
		
		v_s_time=v_s_time+dt
		!-------------------------------
		!First step : transport equation
		!-------------------------------
		call Step1_transport_equation()
		
		!-------------------------------
		!Step 2 : Objective Derivation
		!-------------------------------
		call Get_Sigma_Deux_Tiers_Derivee_Objective(mesh)

		!------------------------------
		!Lagrangian Step
		!------------------------------
		call Lagrangien_Augmente(mesh,ToL)

		!------
		!Step 3
		!------
		call Step3_Strain_Update(mesh)
		
		!----------------------------------
		!Seek min and max to set zrange GIF
		!----------------------------------
		call seek_MinMax(v_x_meshMinMax)
		
		!Print the flow in/out
		call flow(mesh)
		
		print*,"Final time",v_s_time
		
	end do
	!Generate the animation
	call GIF_Animation2(Frame_max,v_x_meshMinMax,Delay,GIF_plot)
	
	call CPU_TIME(t2)
	print*, "Temps total d'execution min, sec : ", floor((t2-t1)/60),t2-t1-60*floor((t2-t1)/60)
	
	!Erase the GIF Directory
	
	deallocate(mesh)
end program NS_Code
