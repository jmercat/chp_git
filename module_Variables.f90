!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!            Module2 is used to define the global variables            !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!***********************************************************************
!
!A general rule for the physical variables definition is applied. 
!The name of the variables is based on the following structure:
!                             
!                           v/p_unit/x_NAME 
!              (v for a variable and p for a parameter)
!              (unit or x for non-dimensional element)
!
!***********************************************************************
module variables
	Implicit none
	
!***********************************************************************
	!The cell type defines the variables on each cell:
	!the Strain, the speed over x (1) and y (2) and the pressure
	type cell
		real*8,dimension(3)::v_Pa_Strain 
		real*8,dimension(2)::v_mps_Speed
		real*8::v_Pa_Pressure  
		integer :: m_mask ! if obstacle : 0
	end type cell
!***********************************************************************
	
	
	!x & y Mesh total element number
	integer::Nx,Ny
	!x & y Element size (spatial steps)
	real*8::dx,dy	
	!Time step
	real*8::dt
	!Time step min
	real*8::dt_min
	!Time step max
	real*8::dt_max
	!Characteristic viscosity time
	real*8::p_s_Tau
	!Fluid viscosity (n_s)
	real*8::p_pas_Viscosity
	!Elastic viscosity (n_p)
	real*8::p_pas_ElasticViscosity
	!Final time
	real*8::p_s_Tmax
	!Set the time variable
	real*8::v_s_time
	!Epsilon
	real*8::p_ppaps_eps
	!r parameter lagrangien augmenté
	real*8::p_pas_r
	!Precision du solveur AX=B
	real*8::Prec_solveur
	!---------------------
	!Linear System values Storage
	!---------------------
	real*8::beta,k1,k2,k3,k4,k5,k6,k7
	!Critère d'arrêt pour le lagrangien augmenté
	real*8::ToL
	!Define the plot type
	integer::plot_type
	!Plot the variables chosen
	integer,dimension(6)::GIF_plot
	!GIF parameter: select the varable displayed
	integer::GIF_parameter
	!Delay parameter to set the ms between each GIF frame
	integer::Delay
	!Set the number of frames in the GIF animation
	integer::frame_number
	!Define the maximum of frames (=number of iteration)
	integer::Frame_max
	!Used to set the GIF zrange (max and min of each variables)
	!1 for the min and 2 for the max
	type(cell),dimension(2)::V_x_meshMinMax
	!The matrix mesh contains the cell elements.  
	type(cell),dimension(:,:),allocatable::mesh
	!The porous field mask
	

end module
	
