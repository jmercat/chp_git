module Mask
use variables
contains
!-----------------------------------------------------------------------------------

!***********************************************************
!               Function Make_mask
!***********************************************************
!Creates field mask, random creation

subroutine Make_mask(Nx, Ny, dx, dy, mesh)
	implicit none
	integer :: k, i, j, nc, nt, nq !nc = circle number, nt= triangle number,nq=...
	integer, intent(in) :: Nx, Ny
	real*8, intent(in) :: dx, dy
	real*8 :: Lx, Ly, at1, at2, at3, aq1, aq2, aq3, aq4, l, cx, cy, radius
	! le paramètre "l" controle la taille des figures générées aléatoirement
	real*8, dimension(:), allocatable :: x, y, vcircle, vtriangle, vquadrangle
	real*8, dimension(:, :), allocatable :: circle, triangle, quadrangle, randcircle, randtriangle
	type(cell),dimension(0:Nx+1,0:Ny+1),intent(inout)::mesh
	
	
	nc=0
	nt=0
	nq=0
	
	allocate(x(0:Nx+1))
	allocate(y(0:Ny+1))

	!allocate(circle(nc, 3))
	!allocate(triangle(nt, 6))
	!allocate(quadrangle(nq, 8))
	
!	allocate(randcircle(nc, 3))
!	allocate(randtriangle(nt, 6))

	mesh(:,:)%m_mask=1

!	l=8

!	do k=1, nc
!		do i=1, 2
!			randcircle(k, i)=100*rand()
!		enddo
!		randcircle(k, 3)=l*rand()
!	enddo		
!	circle=randcircle

!	do k=1, nt
!		do i=1, 2
!			randtriangle(k, i)=100*rand()
!		enddo
!		do i=3, 5, 2
!			randtriangle(k, i)=randtriangle(k, 1)+ l*(2*rand()-1)
!		enddo
!		do i=4, 6, 2
!			randtriangle(k, i)=randtriangle(k, 2) + l*(2*rand()-1)
!		enddo
!	enddo
!	triangle=randtriangle	
	!cx=dx*Nx/2
	!cy=dy*Ny/2
	!radius=dx*Nx/6

	!circle(1, :)=(/cx, cy, radius/)
	!circle(2, :)=(/80, 40, 4/)
	!circle(3, :)=(/15, 80, 6/)
	!circle(4, :)=(/50, 50, 1/)
	!triangle(1, :)=(/10, 10, 30, 20, 15, 30/)
	!triangle(2, :)=(/90, 15, 82, 28, 78, 20/)
	!triangle(1, :)=(/10, 20, 30, 38, 20, 48/)
	!quadrangle(1, :)=(/Nx/2, floor(real(Ny/3)), Nx, floor(real(Ny/3)), Nx, Ny, Nx/2, Ny/)
	!quadrangle(2, :)=(/Nx/2, 1, Nx/2, floor(real(Ny/3)), Nx, floor(real(Ny/3)), Nx/2, 1/)

	Lx=Nx*dx
	Ly=Ny*dy
	do i=0, nx+1
		x(i)=(i-1)*dx
	enddo

	do j=0, ny+1
		y(j)=(j-1)*dy
	enddo


	do i=1, Nx
		mesh(i, ny+1)%m_mask=0
		do j=1, Ny
			if (x(i)>=Lx/2 .and. abs(y(j)-Ly/2)>Ly/6)then
				mesh(i, j)%m_mask=0
			endif
		enddo
	enddo






	!		do k=1, nc
	!			if (((x(i)-circle(k, 1))**2+(y(j)-circle(k, 2))**2)< circle(k, 3)**2) then
	!				mesh(j, i)%m_mask=0
	!			endif
	
	!		enddo
	!do k=1, nt
	!			at1=(triangle(k, 1)-x(i))*(triangle(k, 4)-y(j))-(triangle(k, 2)-y(j))*(triangle(k, 3)-x(i))
	!			at2=(triangle(k, 3)-x(i))*(triangle(k, 6)-y(j))-(triangle(k, 4)-y(j))*(triangle(k, 5)-x(i))
	!			at3=(triangle(k, 5)-x(i))*(triangle(k, 2)-y(j))-(triangle(k, 6)-y(j))*(triangle(k, 1)-x(i))
	!				if (at1*at2>0 .and. at1*at3>0) then
	!					mesh(j, i)%m_mask=0
	!				endif
	!		enddo
	!		do k=1, nq
	!			aq1=(quadrangle(k, 1)-x(i))*(triangle(k, 4)-y(j))-(quadrangle(k, 2)-y(j))*(quadrangle(k, 3)-x(i))
	!			aq2=(quadrangle(k, 3)-x(i))*(triangle(k, 6)-y(j))-(quadrangle(k, 4)-y(j))*(quadrangle(k, 5)-x(i))
	!			aq3=(quadrangle(k, 5)-x(i))*(triangle(k, 8)-y(j))-(quadrangle(k, 6)-y(j))*(quadrangle(k, 7)-x(i))
	!			aq4=(quadrangle(k, 7)-x(i))*(triangle(k, 2)-y(j))-(quadrangle(k, 8)-y(j))*(quadrangle(k, 1)-x(i))
!
	!				if (aq1*aq2>0 .and. aq1*aq3>0 .and. aq1*aq3>0 .and. aq1*aq4>0) then
	!					mesh(j, i)%m_mask=0
	!				endif
	!		enddo
	!	enddo
	!enddo
	!print*, mesh%m_mask

	open(unit = 12, file = 'mask',status = 'unknown', action = 'write')
	do j=1, Ny
		do i=1, Nx
			write(12, *) x(i), y(j), mesh(i, j)%m_mask
	enddo
	enddo
	close(12)

	deallocate(x)
	deallocate(y)
	!deallocate(circle)
	!deallocate(triangle)
	!deallocate(quadrangle)

end subroutine 
!~ !---------------------------------------------------------------------------------------

end module
