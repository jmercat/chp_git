module Step1
use variables
use Shared
contains

!***********************************************************
!                 Function Step1_transport_equation
!***********************************************************
!Step one of the 3 step scheme : transport equation
subroutine Step1_transport_equation()
implicit none
	real*8,dimension(0:Nx,0:Ny,3)::V_Paps_Vgradsigma
	integer::i,j
	
	call prod_VectDivMat(mesh,V_Paps_Vgradsigma)

	do j=1,Ny
		do i=1,Nx
			mesh(i,j)%V_Pa_strain(:)=mesh(i,j)%V_Pa_strain(:) - dt*(V_Paps_VgradSigma(i,j,:))
		end do
	end do
	
	!Update of the boundaries conditions
	call Boundaries_conditions(mesh)
	
!Calculating entry and bottom out of bound values
!~ 	do j=1,Ny
!~ 		mesh(0,j)%V_Pa_Strain=0
!~ 	end do
!~ 	do i=1,Nx
!~ 		mesh(i,0)%V_Pa_Strain=-mesh(i,1)%V_Pa_Strain
!~ 		mesh(i,0)%V_Pa_Strain(3)=mesh(i,1)%V_Pa_Strain(3)
!~ 	end do
end subroutine

subroutine Step1_transport_equation2()
implicit none
	real*8,dimension(0:Nx,0:Ny,3)::V_Paps_Vgradsigma
	integer::i,j
	
	call prod_VectDivMat2(mesh,V_Paps_Vgradsigma)

	do j=1,Ny
		do i=1,Nx
			mesh(i,j)%V_Pa_strain(:)=mesh(i,j)%V_Pa_strain(:) - dt*(V_Paps_VgradSigma(i,j,:))
		end do
	end do
	
	!Update of the boundaries conditions
	call Boundaries_conditions(mesh)
	
	!Calculating entry and bottom out of bound values
!~ 	do j=1,Ny
!~ 		mesh(0,j)%V_Pa_Strain=0
!~ 	end do
!~ 	do i=1,Nx
!~ 		mesh(i,0)%V_Pa_Strain=mesh(i,1)%V_Pa_Strain
!~ 	end do
end subroutine

!calculates V.grad(sigma)
subroutine prod_VectDivMat(mesh,V_Paps_Vgradsigma)
implicit none

	!variable declaration 
	!in
	type(cell),dimension(0:Nx+1,0:Ny+1),intent(in)::mesh
	!out
	real*8,dimension(0:Nx,0:Ny,3),intent(out)::V_Paps_Vgradsigma

	integer::i,j
	
	do j=1,Ny
		do i=1,Nx
			V_Paps_Vgradsigma(i,j,:) = positivePart( mesh(i-1,j)%V_mps_speed(1) ) * Strainflow(i-1,j,1) &
			& + negativePart( mesh(i,j)%V_mps_speed(1) ) * Strainflow(i,j,1) &
			& + positivePart( mesh(i,j-1)%V_mps_speed(2) ) * Strainflow(i,j-1,2) &
			& + negativePart( mesh(i,j)%V_mps_speed(2) ) * Strainflow(i,j,2)
		end do
	end do
end subroutine


subroutine prod_VectDivMat2(mesh,V_Paps_Vgradsigma)
implicit none

	!variable declaration 
	!in
	type(cell),dimension(0:Nx+1,0:Ny+1),intent(in)::mesh
	!out
	real*8,dimension(0:Nx,0:Ny,3),intent(out)::V_Paps_Vgradsigma

	integer::i,j
	
	!calculating the product	
	do j=1,Ny
		do i=1,Nx
			V_Paps_Vgradsigma(i,j,:) = positivePart( mesh(i-1,j)%V_mps_speed(1) ) * Strainflow2m(i,j,1) &
			& + negativePart( mesh(i,j)%V_mps_speed(1) ) * Strainflow2p(i,j,1) &
			& + positivePart( mesh(i,j-1)%V_mps_speed(2) ) * Strainflow2m(i,j,2) &
			& + negativePart( mesh(i,j)%V_mps_speed(2) ) * Strainflow2p(i,j,2)

			!V_Paps_VgradSigma(i,j,:) = positivePart(mesh(i-1,j)%V_mps_speed(1))*(mesh(i,j)%v_Pa_Strain(:)-mesh(i-1,j)%v_Pa_Strain(:))/dx+&
			!&negativePart(mesh(i,j)%V_mps_speed(1))*(mesh(i+1,j)%v_Pa_Strain(:)-mesh(i,j)%v_Pa_Strain(:))/dx+&
			!&positivePart(mesh(i,j-1)%V_mps_speed(2))*(mesh(i,j)%v_Pa_Strain(:)-mesh(i,j-1)%v_Pa_Strain(:))/dy+&
			!&negativePart(mesh(i,j)%V_mps_speed(2))*(mesh(i,j+1)%v_Pa_Strain(:)-mesh(i,j)%v_Pa_Strain(:))/dy

		end do
	end do
end subroutine
	
	function Strainflow(i,j,k)
		implicit none
		integer, intent(in)::i,j,k
		real*8,dimension(3)::Strainflow
		
			!completing the values of the flow
			if (j>=0 .and. j<=Ny) then
				if (i>=0 .and. i<=Nx) then
					if (k==1) then
						Strainflow = ( mesh(i+1,j)%V_Pa_Strain - mesh(i,j)%V_Pa_Strain ) / dx
					elseif (k==2) then
						Strainflow = ( mesh(i,j+1)%V_Pa_Strain - mesh(i,j)%V_Pa_Strain ) / dy
					end if
				end if 
			end if
			
	end function
	
	function Strainflow2p(i,j,k)
		implicit none
		integer, intent(in)::i,j,k
		integer::i2,i1,j2,j1
		real*8,dimension(3)::Strainflow2p
		
		if (i==Nx) then
			i2 = Nx+1
		else
			i2=i+2
		end if
		
		if (j>Ny-1) then
			j2 = Ny+1
		else
			j2=j+2
		end if
		
		 i1 = i+1
		 j1 = j+1

				if (k==1) then	
					Strainflow2p = ( -mesh(i2,j)%V_Pa_Strain + 4.*mesh(i1,j)%V_Pa_Strain - 3.*mesh(i,j)%V_Pa_Strain) / (2.*dx)
				elseif (k==2) then
					Strainflow2p = ( -mesh(i,j2)%V_Pa_Strain + 4.*mesh(i,j1)%V_Pa_Strain - 3.*mesh(i,j)%V_Pa_Strain) / (2.*dy)
				end if

	end function
	
	function Strainflow2m(i,j,k)
		implicit none
		integer, intent(in)::i,j,k
		integer::i2,i1,j2,j1
		real*8,dimension(3)::Strainflow2m
		
		if (i<2) then
			i2 = 0
		else
			i2=i-2
		end if
		i1=i-1
		
		if (j<2) then
			j2 = 0
		else
			j2=j-2
		end if
		j1=j-1

					if (k==1) then	
						Strainflow2m = ( mesh(i2,j)%V_Pa_Strain - 4.*mesh(i1,j)%V_Pa_Strain + 3.*mesh(i,j)%V_Pa_Strain) / (2.*dx)
					elseif (k==2) then
						Strainflow2m = ( mesh(i,j2)%V_Pa_Strain - 4.*mesh(i,j1)%V_Pa_Strain + 3.*mesh(i,j)%V_Pa_Strain) / (2.*dy)
					end if

	end function
	
	

!returns the real value R if R>0 returns 0 otherwise
function positivePart(R)
implicit none
	real*8,intent(in)::R
	real*8::positivePart
	if (R>0) then
		positivePart=R
	else
		positivePart=0
	end if
end function

!returns the real value R if R<0 returns 0 otherwise
function negativePart(R)
implicit none
	real*8,intent(in)::R
	real*8::negativePart
	if (R<0) then
		negativePart=R
	else
		negativePart=0
	end if
end function

end module

