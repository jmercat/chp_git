module Step2
use variables
use Shared
contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!ETAPE 2 - DERIVEE OBJECTIVE!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!~ !---> CALCUL DE SIGMA^(n+2/3) SUR UNE MAILLE <---!

!~ !!ARGUMENTS D'ENTREE -> mesh ( vecteur de cells de taille (Ny+1) x (Nx+1) ATTENTION indexe (0:Ny,0:Nx) )
!~ !!		    			        -> i,j = coordonnees de la maille ( entiers )
!~ !!SORTIE	                        -> mesh(i,j)%V_Pa_Strain = snext = SIGMA^(n+2/3) (vecteur de taille 3)

subroutine Get_Sigma_Deux_Tiers_Derivee_Objective_maille(mesh,i,j)
 implicit none
 
  type(cell),dimension(0:Nx+1,0:Ny+1),intent(inout)::mesh
  integer,intent(in)::i,j
  
  real*8,dimension(2,2)::grad
  real*8,Dimension(17)::constantes
  real*8::det
  real*8,dimension(3,3)::inv,BB,RR
  real*8,dimension(3)::second_membre,s,snext
  integer::k
!On r�cup�re s=sigma^(n+1/3)
s=mesh(i,j)%V_Pa_Strain
 !On calcule grad(v^n)
grad = Get_Gradient_V(mesh,i,j)
  	!~ do k=1,2
		!~ print*,grad(k,1:2)
	!~ enddo
!On calcule les constantes de calcul C1...C17
  constantes=Get_Constantes_Sigma_Deux_Tiers_Derivee_Objective(grad,s)
  !~ do k=1,17
  !~ print*,constantes(k)
  !~ enddo
!On calcule le second membre CG1..CG3
  second_membre(1)=constantes(15)
  second_membre(2)=constantes(16)
  second_membre(3)=constantes(17)
!On calcule inv = A^-1
  det = constantes(1)*constantes(5)*constantes(11) - constantes(1)*constantes(7)*constantes(12) 
  det = det - constantes(6)*constantes(2)*constantes(11)
  inv(1,1)=constantes(5)*constantes(11)-constantes(7)*constantes(12)
  inv(1,2)=-constantes(2)*constantes(11)
  

  inv(1,3)=constantes(2)*constantes(7)
  inv(2,1)= constantes(6)*constantes(12)
  inv(2,2)=-constantes(1)*constantes(12)
  inv(2,3)=constantes(1)*constantes(5)-constantes(2)*constantes(6)
  inv(3,1)=-constantes(6)*constantes(11)
  inv(3,2)=constantes(1)*constantes(11)
  inv(3,3)=-constantes(1)*constantes(7)
  inv=inv/det
  	!~ do k=1,3
		!~ print*,inv(k,1:3)
	!~ enddo
!On calcule snext = inv*second_membre
   !~ print*," avant ",s
   snext =matmul(inv,second_membre)
   !~ print*,snext
   !~ print*
       open(unit=1, file='sigma.dat', form='formatted')  !! Ouverture d'un fichier texte pour l'écriture des données
	write(1,*) "Maille  (i,j)= ",i,j, "	                              Sigma = ", snext(1) , " " , snext(2) , " ",snext(3) 
  
  BB(1,1)=constantes(3)
  BB(1,2)=0
  BB(1,3)=constantes(4)
  BB(2,1)= constantes(9)
  BB(2,2)=constantes(10)
  BB(2,3)=constantes(8)
  BB(3,1)=0
  BB(3,2)=constantes(13)
  BB(3,3)=constantes(14)
   RR = matmul(inv,BB)
   	!~ do k=1,3
		!~ print*,RR(k,1:3)
	!~ enddo
	!~ print*
  
!On copie snext dans mesh
mesh(i,j)%V_Pa_Strain=snext

end subroutine Get_Sigma_Deux_Tiers_Derivee_Objective_maille

!_____________________________________________________________________________________________________________

!~ !---> CALCUL DE SIGMA^(n+2/3)  <---!

!~ !!ARGUMENTS D'ENTREE -> mesh ( vecteur de cells de taille (Ny+1) x (Nx+1) ATTENTION indexe (0:Ny,0:Nx) )
!~ !!SORTIE	                        -> mesh(:,:)%V_Pa_Strain = snext = SIGMA^(n+2/3) (vecteur de taille 3)

subroutine Get_Sigma_Deux_Tiers_Derivee_Objective(mesh)
 implicit none
 
  type(cell),dimension(0:Nx+1,0:Ny+1),intent(inout)::mesh
  integer::i,j
  
  do j=1,Ny
	do i=1,Nx
		call Get_Sigma_Deux_Tiers_Derivee_Objective_maille(mesh,i,j)
	enddo
  enddo
  call Boundaries_conditions(mesh)

end subroutine Get_Sigma_Deux_Tiers_Derivee_Objective

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!CALCUL DE GRAD(V^n)!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!ARGUMENTS D'ENTREE -> mesh ( vecteur de cells de taille (Nx+1) x (Ny+1) ATTENTION indexe (0:Nx,0:Ny) )
!!		     			   -> i,j = coordonnees de la maille ( entiers )
!!SORTIE 	     		   -> grad = GRAD(V^n) (matrice de taille 2x2)

function Get_Gradient_V(mesh,i,j) RESULT (grad)
 implicit none

  type(cell),dimension(0:Nx+1,0:Ny+1)::mesh
  integer::i,j
  real*8,dimension(2,2)::grad
  real*8::ubef,unext,wbef,wnext,uplus,umoins,wplus,wmoins

! ----> Calcul de grad(1,1)= du/dx (schema centre)
  ubef = mesh(i-1,j)%V_mps_Speed(1)	
  unext = mesh(i,j)%V_mps_Speed(1)	
  grad(1,1) = (unext - ubef)/dx 
 
! ----> Calcul de grad(1,2)= du/dy (schema centre)
!if(j==Ny) then 
!uplus=0
!else
uplus= (mesh(i-1,j+1)%V_mps_Speed(1)+mesh(i+1,j+1)%V_mps_Speed(1)+mesh(i-1,j)%V_mps_Speed(1)+mesh(i,j)%V_mps_Speed(1) )/4.
!endif
umoins = (mesh(i-1,j)%V_mps_Speed(1)+mesh(i,j)%V_mps_Speed(1)+mesh(i-1,j-1)%V_mps_Speed(1)+mesh(i+1,j-1)%V_mps_Speed(1) )/4.
grad(1,2) = (uplus - umoins)/dy

! ----> Calcul de grad(2,1)= dw/dx (schema centre)
!if(i==Nx) then 
!wplus=(mesh(i,j)%V_mps_Speed(2)+mesh(i-1,j)%V_mps_Speed(2))/2.
!else
wplus= (mesh(i,j)%V_mps_Speed(2) + mesh(i+1,j)%V_mps_Speed(2) + mesh(i+1,j-1)%V_mps_Speed(2) + mesh(i,j-1)%V_mps_Speed(2)  )/4.
!endif
wmoins = (mesh(i,j)%V_mps_Speed(2) + mesh(i-1,j)%V_mps_Speed(2) + mesh(i-1,j-1)%V_mps_Speed(2) + mesh(i,j-1)%V_mps_Speed(2)  )/4.

  grad(2,1) = (wplus - wmoins)/dx
  
! ----> Calcul de grad(2,2)= dw/dy (schema centre)
  wbef = mesh(i,j-1)%V_mps_Speed(2)
  wnext = mesh(i,j)%V_mps_Speed(2)
  grad(2,2) = (wnext - wbef)/dy

end function Get_Gradient_V
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!CALCUL DES CONSTANTES C_i POUR LE CALCUL DE SIGMA^(2/3)!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!ARGUMENTS D'ENTREE -> grad =  GRAD(V^n) (matrice de taille 2x2)
!!		   			   -> s = SIGMA^(n+1/3) (vecteur de taille 3)
!!SORTIE	     			   -> constantes (vecteur de taille 17, [|C1,C2,C3,C4,C5,C6,C7,C8,C9,C10,C11,C12,C13,C14,CG1,CG2,CG3|])

function Get_Constantes_Sigma_Deux_Tiers_Derivee_Objective(grad,s) result (constantes)
implicit none 

  real*8,dimension(2,2)::grad
  real*8,dimension(3)::s
  real*8,Dimension(17)::constantes
  
  constantes(1)= 1-dt*grad(1,1)
  constantes(2)= -dt*grad(1,2)
  constantes(3)= 1+dt*grad(1,1)
  constantes(4)= dt*grad(1,2)
  constantes(5)= 2-dt*(grad(1,1)+grad(2,2))
  constantes(6)= -dt*grad(2,1)
  constantes(7)= -dt*grad(1,2)
  constantes(8)= 2+dt*(grad(1,1)+grad(2,2))
  constantes(9)= dt*grad(2,1)
  constantes(10)= dt*grad(1,2)
  constantes(11)= 1-dt*grad(2,2)
  constantes(12)= -dt*grad(2,1)
  constantes(13)= 1+dt*grad(2,2)
  constantes(14)= dt*grad(2,1)
  constantes(15)= constantes(3)*s(1) + constantes(4)*s(3)
  constantes(16)= constantes(8)*s(3) + constantes(9)*s(1) + constantes(10)*s(2)
  constantes(17)= constantes(13)*s(2) + constantes(14)*s(3)
  
  end function Get_Constantes_Sigma_Deux_Tiers_Derivee_Objective
  
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!CALCUL DE SIGMAtilde^(2/3)(i,j)(1,2) POUR B1!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!ARGUMENTS D'ENTREE -> mesh ( vecteur de cells de taille (Nx+1) x (Ny+1) ATTENTION indexe (0:Nx,0:Ny) )
!!		     			   -> i,j = coordonnees de la maille ( entiers )
!!SORTIE 	     		   -> stilde(i,j)(1,2) pour le calcul de B1 (second membre selon X)= r�el

!/!\ CETTE FONCTION FAIT APPEL A SIGMA^(n+2/3), IL FAUT DONT Y FAIRE APPEL UNE FOIS CETTE QUANTITE CALCULEE

Function Get_Sigma_Tilde_12(mesh,i,j) Result(stilde)
implicit none

  type(cell),dimension(0:Nx+1,0:Ny+1)::mesh
  integer::i,j
  real*8::stilde

	stilde=0.25*(mesh(i,j+1)%V_Pa_Strain(3)+mesh(i+1,j+1)%V_Pa_Strain(3)+mesh(i,j)%V_Pa_Strain(3)+mesh(i+1,j)%V_Pa_Strain(3))

end function



end module 
